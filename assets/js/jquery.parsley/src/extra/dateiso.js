/*
 * Copyright (c) 2015. The content in this file is Protected by the copyrightaws of kenya and owned by Api Craft Technology.
 *  Reproducing it in any way or using it without permission from Api Craft Technology will be a violation of kenyan copyrights law.
 *  This may be subject to prosecution according to the kenyan law.
 */

// dateiso extra validator
// Guillaume Potier
window.ParsleyConfig = window.ParsleyConfig || {};
window.ParsleyConfig.validators = window.ParsleyConfig.validators || {};

window.ParsleyConfig.validators.dateiso = {
  fn: function (value) {
    return /^(\d{4})\D?(0[1-9]|1[0-2])\D?([12]\d|0[1-9]|3[01])$/.test(value);
  },
  priority: 256
};
