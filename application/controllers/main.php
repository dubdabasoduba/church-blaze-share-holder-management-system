<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Main extends CI_Controller {

	public function addAgents() {
		if ($this->session->userdata('is_logged_in')) {
			$index['mainContent'] = 'Admin/agents/addAgents';
			$index['title'] = "Add Agents";
			$this->load->view('Admin/adminRedirect', $index);
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
	
	public function viewAgents() {
		if ($this->session->userdata('is_logged_in')) {
			$index['mainContent'] = 'Admin/agents/viewAgents';
			$index['title'] = "View Agents";
			$this->load->view('Admin/adminRedirect', $index);
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
	
	public function addShareholders() {
		if ($this->session->userdata('is_logged_in')) {
			$index['mainContent'] = 'Admin/shareholders/addShareholders';
			$index['title'] = "Add Agents";
			$this->load->view('Admin/adminRedirect', $index);
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function viewShareholders() {
		if ($this->session->userdata('is_logged_in')) {
			$index['mainContent'] = 'Admin/shareholders/viewShareholders';
			$index['title'] = "View Agents";
			$this->load->view('Admin/adminRedirect', $index);
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
	
	public function addPayment() {
		if ($this->session->userdata('is_logged_in')) {
			$index['mainContent'] = 'Admin/shareholders/addShareholders';
			$index['title'] = "Add Agents";
			$this->load->view('Admin/adminRedirect', $index);
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function viewPayment() {
		if ($this->session->userdata('is_logged_in')) {
			$index['mainContent'] = 'Admin/shareholders/viewShareholders';
			$index['title'] = "View Agents";
			$this->load->view('Admin/adminRedirect', $index);
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
	
	public function userProfile() {
		if ($this->session->userdata('is_logged_in')) {
			$index['mainContent'] = 'Admin/userProfile';
			$index['title'] = "User Profile";
			$this->load->view('Admin/adminRedirect', $index);
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function restricted() {
		$this->load->view('restricted');
	}

	public function signup_validation() {
		$this->load->library('form_validation');

		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[users.email]');

		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|matches[password]');

		$this->form_validation->set_message('is_unique', "This email has already been taken");

		if ($this->form_validation->run()) {

			//generate random key
			$key = md5(uniqid());

			//send email to user
			$this->load->library('email');
			$this->load->model('model_users');
			$this->email->from('support@cbss.com', 'Support');
			$this->email->to($this->input->post('email'));
			$this->email->subject("Confirm your account");

			$message = "<p>Thankyou for registering with us.</p>";
			$message .= "<p><a href='" . base_url()
					. "main/register_user/$key' >Click here </a> to confirm your account and login in to your new CB Shareholders account.</p>";

			$this->email->message($message);

			if ($this->model_users->add_temp_users($key)) {
				if ($this->email->send()) {
					echo "The confirmation email has been send to your account.";
				} else {
					echo "Sorry, we did not successfully verify you email.";
				}
			} else {
				echo "Problem registering user";
			}

			//add users to the temp db

		} else {
			echo "You shall not pass! >:(";
			$this->load->view('signup');
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		echo "Sucessfully Logged out";
		redirect('main/login');
	}

	public function register_user($key) {
		$this->load->model('model_users');

		if ($this->model_users->is_key_valid($key)) {
			if ($newemail = $this->model_users->add_user($key)) {
				$data = array ( 'email' => $newemail, 'is_logged_in' => 1 );
				$this->session->set_userdata($data);
				redirect('main/members');
			} else {
				echo "User registration failed. Try again later.";
			}
		} else {
			echo "Invalid Key";
		}
	}

}

