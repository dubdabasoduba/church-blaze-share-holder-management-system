<body class="texture">

<div id="cl-wrapper" class="error-container">
	<div class="page-error">
		<h1 class="number text-center">Access Denied</h1>

		<h2 class="description text-center">Sorry, but you do not have access to this page!</h2>

		<h3 class="text-center">Go <a style="color:#BAD532;" href="<?php echo base_url(); ?>">Home </a>?</h3>
	</div>


</div>

