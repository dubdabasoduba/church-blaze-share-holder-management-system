<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Shares extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('shareholders/shareholders_model', 'shareholders');
		$this->load->model('shares/shares_model', 'shares');
		$this->load->model('settings/settings_model', 'settings');
	}

	public function dashboard()
	{
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
			OR ($this->session->userdata('role') === "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'shareDashboard';
				$data['title'] = "Shares Subscription Dashboard";
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function viewShares()
	{
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
			OR ($this->session->userdata('role') === "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'viewShares';
				$data['title'] = "View Shares";
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function receipt($buyId = null)
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
			OR ($this->session->userdata('role') === 'Finance') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				if ($buyId === null) {
					show_error('No shit found', 500);
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['mainContent'] = 'shareReport';
					$data['title'] = "Print Receipt";
					$data['view_data1'] = $this->settings->editSysInfo1();
					$result = $this->shares->editGet($buyId);
					$data['view_data'] = $result;
					$price = $this->shares->getsharetypePrice($buyId);
					$data['price'] = $price;
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function viewSearch($sort_by = 'Receiptnumber', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
			OR ($this->session->userdata('role') === 'Finance')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");
				$limit = 5;
				$data['fields'] = array("physicalFormNumber" => "CB form Number", "Receiptnumber" => "Subscription Code",
					"shareTypeName" => "Share Series", "shareNumber" => "Subscribed Shares",
					"expectedAmount" => "Expected Payment", "datebought" => "Date Subscribed",
					"Name" => "Shareholder Name");
				$idnumber = $this->input->post('search');
				$result = $this->shares->getShares($idnumber, $limit, $offset, $sort_by, $sort_order);
				$data['mainContent'] = 'viewShares';
				$data['title'] = "View Shares";
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("shares/viewSearch/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				if ($result['rows'] == null) {
					$data['error'] = ("No share subscription tied to the specified user was found");
				} else {
					$data['success'] = ("Share subscription was retrieved successfully");
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function modify($buyId = null)
	{
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
			OR ($this->session->userdata('role') === 'Finance')
		) {
			if ($this->session->userdata('logged_in')) {
				if ($buyId === null) {
					show_error('No shit found', 500);
				} else {

					$result1 = $this->shares->getshareTypes();
					$index['view_data1'] = $result1['rows'];
					$result = $this->shares->editGet($buyId);
					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$index['mainContent'] = 'editShares';
					$index['title'] = "Edit Subscribed Shares";
					$index['view_data'] = $result;
					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function buyShares()
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
			OR ($this->session->userdata('role') === 'Finance') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$result1 = $this->shares->getshareTypes();
				$data['view_data1'] = $result1['rows'];
				/*$index = null;
				$result = $this->shareholders->get_search($index);*/
				$data['mainContent'] = 'buyShares';
				$data['title'] = "Subscribe Shares";
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function holderSearch()
	{
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
			OR ($this->session->userdata('role') === "Agent") OR ($this->session->userdata('role') === "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$index = $this->input->post('search');
				$result1 = $this->shares->getshareTypes();
				$data['view_data1'] = $result1['rows'];
				$result = $this->shareholders->get_search($index);
				$data['mainContent'] = 'buyShares';
				$data['title'] = "Subscribe Shares";
				$data['view_data'] = $result;
				if ($result == null) {
					$data['error'] =
						("No Share holder with the specified Id Number was found. Make sure that the user us Approved");
				} else {
					$data['success'] = ("Share holder was retrieved successfully");
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function remove($buyId = null)
	{
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
			OR ($this->session->userdata('role') === 'Finance')
		) {
			if ($this->session->userdata('logged_in')) {
				if ($buyId === null) {
					show_error('No shit provided', 500);
				} else {

					$typeID = $this->shares->getsharetypeId($buyId);
					$total = $this->shares->getshareTotal($buyId);
					$sold = $this->shares->getshareTotalSold($buyId);
					$boughtshares = $this->shares->getsharesBought($buyId);
					$newtotal = $total + $boughtshares;
					$newsold = $sold - $boughtshares;
					$index['Id'] = $typeID;
					$index['sharetotal'] = $newtotal;
					$index['sharesold'] = $newsold;
					$this->shares->updateShareTypes($index);
					$this->shares->remove($buyId);
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['success'] = ("Shares bought were deleted successfully");
					header('Location:' . base_url('shares/viewShares', $data));
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function save()
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
			OR ($this->session->userdata('role') === 'Finance') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('shares', 'Number of shares', 'required|trim');
				$this->form_validation->set_rules('fnumber', 'Physical Form Number', 'required|trim');
				$idnumber = $this->input->post('idnumber');
				$kra = $this->input->post('kra');
				$type = $this->input->post('type');
				$typeuser = $this->input->post('typeuser');
				$shares = $this->input->post('shares');
				$fnumber = $this->input->post('fnumber');
				if ($type != 0) {
					if (($this->form_validation->run()) AND ($shares != 0)) {
						$receipt = $this->shares->ReceiptNumGen();
						$total = $this->shares->getTotal($type);
						$sold = $this->shares->getSold($type);
						$amount = $this->shares->getAmount($type);
						$amountSubscribed = $this->shares->getTotalSharesSubscriptionAmount($type);
						$desc = $this->shares->getDesc($type);
						$name = $this->shares->getName($type);
						$sharetype = $this->shares->get_shareType($type);
						$avail = ($total - $sold);
						$newsold = ($sold + $shares);

						if ($shares < $avail OR $shares == $avail) {
							if (isset($_POST) && $_POST['add'] === 'Buy Shares') {
								$index['shareholder'] = $this->input->post('Id');
								if ($typeuser == 0) {
									$index['idnumber'] = $idnumber;
								} else {
									$index['idnumber'] = $kra;
								}
								$index['sharetype'] = $type;
								$index['shareTypeName'] = $sharetype;
								$index['shareNumber'] = $shares;
								$index['physicalFormNumber'] = $fnumber;
								$index['expectedAmount'] = $expected = ($amount * $shares);
								$index['Receiptnumber'] = $receipt;
								$index['AddedBy'] = $this->input->post('addedby');
								$index['dateadded'] = date("Y-m-d");
								$this->shares->add($index);
								$user['Id'] = $type;
								$user['sharesold'] = $newsold;
								$user['shareAmountExpected'] = $total = (($amount * $shares) + $amountSubscribed);
								$this->shares->updateShareType($user);
								$data['success'] = ("");
								$data['error'] = ("");
								$data['info'] = ("");
								$data['mainContent'] = 'shareReport';
								$data['title'] = "Print Receipt";
								$data['view_data1'] = $this->settings->editSysInfo1();
								$result = $this->shares->editGetReceipt($receipt);
								$data['view_data'] = $result;
								$price = $this->shares->getsharetypePriceReceipt($receipt);
								$data['price'] = $price;

								$this->load->view('Admin/adminRedirect', $data);
							} else {
								$data['success'] = ("");
								$data['error'] = ("");
								$data['info'] = ("");
								$result1 = $this->shares->getshareTypes();
								$data['view_data1'] = $result1['rows'];
								$result = $this->shareholders->get_search($idnumber);
								$data['mainContent'] = 'buyShares';
								$data['title'] = "Subscribe Shares";
								$data['view_data'] = $result;
								$data['error'] = ("Shares Could Not be bought. Contact System Admin");

								$this->load->view('Admin/adminRedirect', $data);
							}
						} else {
							$data['success'] = ("");
							$data['error'] = ("");
							$data['info'] = ("");
							$result1 = $this->shares->getshareTypes();
							$data['view_data1'] = $result1['rows'];
							$result = $this->shareholders->get_search($idnumber);
							$data['mainContent'] = 'buyShares';
							$data['title'] = "Subscribe Shares";
							$data['view_data'] = $result;
							$data['error'] =
								("The number of Shares reminaing are less than entered. The available shares are = "
									. $avail . " Shares ");

							$this->load->view('Admin/adminRedirect', $data);
						}

					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$result1 = $this->shares->getshareTypes();
						$data['view_data1'] = $result1['rows'];
						$result = $this->shareholders->get_search($idnumber);
						$data['mainContent'] = 'buyShares';
						$data['title'] = "Subscribe Shares";
						$data['view_data'] = $result;
						$data['error'] = ("Please fill all the spaces appropriately");

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$result1 = $this->shares->getshareTypes();
					$data['view_data1'] = $result1['rows'];
					$result = $this->shareholders->get_search($idnumber);
					$data['mainContent'] = 'buyShares';
					$data['title'] = "Subscribe Shares";
					$data['view_data'] = $result;
					$data['error'] = ("Please Choose a share type for you to buy shares");

					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function update()
	{
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
			OR ($this->session->userdata('role') === 'Finance')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('shares', 'Number of shares', 'required|trim');
				$this->form_validation->set_rules('fnumber', 'Physical Form Number', 'required|trim');
				$idnumber = $this->input->post('idnumber');
				$buyId = $this->input->post('buyId');
				$type = $this->input->post('type');
				$shares = $this->input->post('shares');
				$subExpectedAmount = $this->input->post('expected');
				$sharesNumber = $this->input->post('sharenumber');
				$fnumber = $this->input->post('fnumber');
				if ($type != 0) {
					if (($this->form_validation->run()) AND ($shares !== 0)) {
						$receipt = $this->input->post('receipt');
						$total = $this->shares->getTotal($type);
						$sold = $this->shares->getSold($type);
						$amount = $this->shares->getAmount($type);
						$sharetype = $this->shares->get_shareType($type);
						$amountSubscribed = $this->shares->getTotalSharesSubscriptionAmount($type);
						$newamountSubscribed = $amountSubscribed - $subExpectedAmount;
						$avail = ($total - $sold);
						$newsold = (($sold - $sharesNumber) + $shares);

						if ($shares < $avail OR $shares == $avail) {
							if (isset($_POST) && $_POST['edit'] === 'Edit Shares') {
								$index['buyId'] = $buyId;
								$index['shareholder'] = $this->input->post('Id');
								$index['idnumber'] = $idnumber;
								$index['sharetype'] = $type;
								$index['shareTypeName'] = $sharetype;
								$index['shareNumber'] = $shares;
								$index['physicalFormNumber'] = $fnumber;
								$index['expectedAmount'] = $expected = ($amount * $shares);
								$index['Receiptnumber'] = $receipt;
								$index['AddedBy'] = $this->input->post('addedby');
								$index['dateadded'] = $this->input->post('dateadded');
								$index['UpdatedBy'] = $this->input->post('Updatedby');
								$index['dateupdated'] = date("Y-m-d");
								$this->shares->add($index);
								$user['Id'] = $type;
								$user['sharetotal'] = $total;
								$user['sharesold'] = $newsold;
								$user['shareAmountExpected'] = $total = (($amount * $shares) + $newamountSubscribed);
								$this->shares->updateShareType($user);
								$data['success'] = ("");
								$data['error'] = ("");
								$data['info'] = ("");
								$data['mainContent'] = 'shareReport';
								$data['title'] = "Print Receipt";
								$data['view_data1'] = $this->settings->editSysInfo1();
								$result = $this->shares->editGetReceipt($receipt);
								$data['view_data'] = $result;
								$price = $this->shares->getsharetypePriceReceipt($receipt);
								$data['price'] = $price;

								$this->load->view('Admin/adminRedirect', $data);
							} else {
								$result1 = $this->shares->getshareTypes();
								$data['view_data1'] = $result1['rows'];
								$result = $this->shares->editGet($buyId);
								$data['success'] = ("");
								$data['error'] = ("");
								$data['info'] = ("");
								$data['mainContent'] = 'editShares';
								$data['title'] = "Edit Shares";
								$data['view_data'] = $result;
								$data['error'] = ("Shares could not be updated");
								$this->load->view('Admin/adminRedirect', $data);
							}
						} else {
							$result1 = $this->shares->getshareTypes();
							$data['view_data1'] = $result1['rows'];
							$result = $this->shares->editGet($buyId);
							$data['success'] = ("");
							$data['error'] = ("");
							$data['info'] = ("");
							$data['mainContent'] = 'editShares';
							$data['title'] = "Edit Subscribed Shares";
							$data['view_data'] = $result;
							$data['error'] =
								("The number of Shares reminaing are less than entered. The available shares are = "
									. $avail . " Shares ");

							$this->load->view('Admin/adminRedirect', $data);
						}

					} else {
						$result1 = $this->shares->getshareTypes();
						$data['view_data1'] = $result1['rows'];
						$result = $this->shares->editGet($buyId);
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'editShares';
						$data['title'] = "Edit Subscribed Shares";
						$data['view_data'] = $result;
						$data['error'] = ("Please fill all the spaces appropriately");

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$result1 = $this->shares->getshareTypes();
					$data['view_data1'] = $result1['rows'];
					$result = $this->shares->editGet($buyId);
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['mainContent'] = 'editShares';
					$data['title'] = "Edit Subscribed Shares";
					$data['view_data'] = $result;
					$data['error'] = ("Please Choose a share type for you to buy shares");

					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function search($sort_by = 'Id', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Finance")
			OR ($this->session->userdata('role') === "Agent")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$limit = 5;
				$data['fields'] = array("Id" => "Id", "Name" => "Names", "IdNo" => "Id Number", "Telephone" => "Telephone",
					"Email" => "Email");

				$index = $this->input->post('search');
				$result = $this->shareholders->get_search($index, $limit, $offset, $sort_by, $sort_order);
				$data['mainContent'] = 'buyShares';
				$data['title'] = "Buy Shares";
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];

				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("shareholders/buyShares/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;

				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
}
