<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Reports_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getShareholders()
	{
		$q = $this->db->select('*')->from('shareholders');
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function getShares($idnumber)
	{
		$q = $this->db->select('*')->from('shareholders')->join('sharesBought as sG', 'sG.shareholder = shareholders.Id')
			->where('idnumber', $idnumber);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function get2($receipt)
	{
		$q = $this->db->select('*')->from('sharetypes')->join('payment', 'payment.shareType = sharetypes.Id')
			->where('buyreceipt', $receipt);

		$query = $q->get()->result();
		$result['rows'] = $query;
		return $result;

	}

	public function getSubscribedShares($dateone, $datetwo)
	{
		$q = $this->db->select('*')->from('shareholders')->join('sharesBought as sG', 'sG.shareholder = shareholders.Id')
			->where('sG.datebought >', $dateone)->where('sG.datebought <', $datetwo);
		$query = $q->get()->result();
		$result['rows'] = $query;
		return $result;
	}

	function totalSubscribedShares($dateone, $datetwo)
	{
		$result =
			mysql_query("SELECT SUM(shareNumber) AS shares_sum FROM sharesBought WHERE  datebought >= '$dateone' AND datebought <= '$datetwo'");
		$row = mysql_fetch_assoc($result);
		$totalShares = $row['shares_sum'];

		return $totalShares;
	}

	function totalAmountSubscribed($dateone, $datetwo)
	{
		$result =
			mysql_query("SELECT SUM(expectedAmount) AS expectedAmount FROM sharesBought WHERE datebought >= '$dateone' AND datebought <= '$datetwo'");
		$row = mysql_fetch_assoc($result);
		$totalAmount = $row['expectedAmount'];

		return $totalAmount;
	}

	public function getShareholdersDef($dateone, $datetwo)
	{
		$q = $this->db->select('*')->from('shareholders')->where('type =', 0)->where('dateadded >', $dateone)
			->where('dateadded <', $datetwo);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	function totalShareholdersDef($dateone, $datetwo)
	{
		$q1 = $this->db->select('COUNT(*) as count', false)->from('shareholders')->where('type =', 0)
			->where('dateadded >=', $dateone)->where('dateadded <=', $datetwo);
		$tmp = $q1->get()->result();
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}

	public function getCompanyShareholdersDef($dateone, $datetwo)
	{
		$q = $this->db->select('*')->from('shareholders')->where('type =', 1)->where('dateadded >', $dateone)
			->where('dateadded <', $datetwo);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	function totalCompanyShareholdersDef($dateone, $datetwo)
	{
		$q1 = $this->db->select('COUNT(*) as count', false)->from('shareholders')->where('type =', 1)
			->where('dateadded >=', $dateone)->where('dateadded <=', $datetwo);
		$tmp = $q1->get()->result();
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}

	public function getAllAgents()
	{
		$q = $this->db->select('*')->from('agents');
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function getAgentsOverTime($dateone, $datetwo)
	{
		$q = $this->db->select('*')->from('agents')->where('dateadded >= ', $dateone)->where('dateadded <=', $datetwo);
		$query = $q->get()->result();
		$result['rows'] = $query;
		return $result;
	}

	public function getAgentsOverTimeCount($dateone, $datetwo)
	{
		$q = $this->db->select('COUNT(*) as count', false)->from('agents')->where('dateadded >= ', $dateone)->where('dateadded <=', $datetwo);
		$query = $q->get()->result();
		$result['row_count'] = $query[0]->count;
		return $result;
	}

	public function get_agentsdropdown()
	{
		$q = $this->db->select('*')->from('agents')->order_by('AgName');
		$query = $q->get()->result();
		$result['rows'] = $query;

		return $result;
	}

	public function getShareholderagent($dateone, $datetwo, $agentnumber)
	{
		$q = $this->db->select('*')->from('shareholders')->join('agents as ag', 'ag.Id = shareholders.agentID')
			->where('ag.Number =', $agentnumber)->where('shareholders.dateadded >', $dateone)
			->where('shareholders.dateadded <', $datetwo);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function getTotalShareholderagent($dateone, $datetwo, $agentnumber)
	{
		$q = $this->db->select('COUNT(*) as count', false)->from('shareholders')
			->join('agents as ag', 'ag.Id = shareholders.agentID')->where('ag.Number =', $agentnumber)
			->where('shareholders.dateadded >', $dateone)->where('shareholders.dateadded <', $datetwo);
		$tmp = $q->get()->result();
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}

	public function getAllShareholderAgent($agentnumber)
	{
		$q = $this->db->select('*')->from('shareholders')->join('agents as ag', 'ag.Id = shareholders.agentID')
			->where('ag.Number =', $agentnumber);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function getAllTotalShareholderagent($agentnumber)
	{
		$q = $this->db->select('COUNT(*) as count', false)->from('shareholders')
			->join('agents as ag', 'ag.Id = shareholders.agentID')->where('ag.Number =', $agentnumber);
		$tmp = $q->get()->result();
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}

	public function getShareRegister($dateone, $datetwo)
	{
		$sql = "select shareholders.physicalFormNumber As physical_form_number,shareholders.formnumber As System_form_number,
shareholders.Name,
shareholders.IdNo,
shareholders.krapin,
shareholders.cin,
shareholders.Telephone,
shareholders.box,
shareholders.Email,
shareholders.Country,
shareholders.County,
shareholders.Town,
shareholders.Village,
shareholders.dateadded,
sharetypes.type,
sharetypes.seriesPrice,
sharesBought.shareNumber,
sharesBought.expectedAmount,
sharesBought.Receiptnumber As system_subcscription_code,
payment.physicalFormNumber As Physical_receipt_number,
payment.date,
payment.Amount,
payment.numberofshares As Shares_paid_for,
payment.PaymentType,
payment.Code,
payment.PaymentUUID As system_payment_code

from shareholders
left join sharesBought on
sharesBought.shareholder = shareholders.Id
left join sharetypes on
sharetypes.Id = sharesBought.shareType
left join payment on
sharesBought.buyId = payment.buyId
where shareholders.dateadded BETWEEN " . $dateone . "  AND " . $datetwo;
		$q = $this->db->query($sql);
		$query = $q->result();

		$result['rows'] = $query;
	}

	public function demo($dateone, $datetwo)
	{
		$q = $this->db->select('*')->from('agents')->order_by('AgName');
		$query = $q->get()->result();
		$result['rows'] = $query;

		return $result;
	}

	public function getPayments($dateone, $datetwo)
	{
		$q = $this->db->select('*')->from('shareholders')->join('payment as pay', 'pay.holder = shareholders.Id')
			->join('sharesBought as sG', 'sG.shareholder = pay.buyId')
			->where('sG.datebought >', $dateone)->where('sG.datebought <', $datetwo);
		$query = $q->get()->result();
		$result['rows'] = $query;
		return $result;
	}

	function totalAmountPaid($dateone, $datetwo)
	{
		$result =
			mysql_query("SELECT SUM(Amount) AS amount FROM payment WHERE date >= '$dateone' AND date <= '$datetwo'");
		$row = mysql_fetch_assoc($result);
		$totalAmount = $row['amount'];

		return $totalAmount;
	}

	function totalSubscribedSharesPaid($dateone, $datetwo)
	{
		$result =
			mysql_query("SELECT SUM(numberofshares) AS shares_sum FROM payment WHERE  date >= '$dateone' AND date <= '$datetwo'");
		$row = mysql_fetch_assoc($result);
		$totalShares = $row['shares_sum'];

		return $totalShares;
	}
}
