<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					General Reporting
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;General Reporting</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="alert alert-info">
				<i class="fa fa-info-circle"></i>
				<strong>Heads up!</strong>
				All general Reports from the system are to be handled here!
			</div>
			<hr/>
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a data-toggle="tab" href="#shareholderReports"><h5><i class="fa fa-users"></i>&nbsp;
							Shareholder Reports</h5></a></li>
				<li><a data-toggle="tab" href="#sharesReports"><h5><i class="fa fa-file-code-o"></i>&nbsp;Shares Reports
						</h5>
					</a>
				</li>
				<li><a data-toggle="tab" href="#agentsReports"><h5><i class="fa fa-user-plus"></i>&nbsp;Agents Reports</h5>
					</a>
				</li>
				<!--<li><a data-toggle="tab" href="#finanicialReports"><h5><i class="fa fa-money"></i>&nbsp;Financial
							Reports</h5></a>
				</li>-->
			</ul>
			<div class="tab-content">
				<div id="shareholderReports" class="tab-pane fade in active">
					<hr/>
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-wisteria">
								<div class="panel-body">

									<a href="<?php echo base_url() . "reports/viewShareholders" ?>"><i
											class="fa fa-users fa-5x bg-color-wisteria"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-wisteria">
									<a class="white" href="<?php echo base_url() . "reports/viewShareholders"
									?>">All Shareholders</a>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-green">
								<div class="panel-body">
									<a href="<?php echo base_url() . "reports/viewIndividualShareholdersReport" ?>"><i
											class="fa fa-user fa-5x bg-color-green"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-green">
									<a class="white"
									   href="<?php echo base_url() . "reports/viewIndividualShareholdersReport"
									   ?>">Individual Shareholders By Defined Dates</a>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-TribalTriumph">
								<div class="panel-body">
									<a href="<?php echo base_url() . "reports/viewCompanyShareholdersReport" ?>"><i
											class="fa fa-user fa-5x bg-color-TribalTriumph"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-TribalTriumph">
									<a class="white"
									   href="<?php echo base_url() . "reports/viewCompanyShareholdersReport"
									   ?>">Company Shareholders By Defined Dates</a>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-mwendwayellow">
								<div class="panel-body">
									<a href="<?php echo base_url() . "reports/share_register" ?>"><i
											class="fa fa-file-excel-o fa-5x bg-color-mwendwayellow"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-mwendwayellow">
									<a class="white"
									   href="<?php echo base_url() . "reports/share_register"
									   ?>">The Share Register Download</a>
								</div>
							</div>
						</div>
					</div>
					<hr/>
				</div>
				<div id="sharesReports" class="tab-pane fade">
					<hr/>
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-sunflower">
								<div class="panel-body">
									<a href="<?php echo base_url() . "reports/viewShares" ?>"><i
											class="fa fa-dollar fa-5x bg-color-sunflower"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-sunflower">
									<a class="white" href="<?php echo base_url() . "reports/viewShares"
									?>">Individual Share Subscription</a>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-royalpurple">
								<div class="panel-body">
									<a href="<?php echo base_url() . "reports/viewPayment" ?>"><i
											class="fa fa-bar-chart fa-5x bg-color-royalpurple"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-royalpurple">
									<a class="white" href="<?php echo base_url() . "reports/viewPayment"
									?>">Individual Share Payment</a>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-lavetafebbraio">
								<div class="panel-body">
									<a href="<?php echo base_url() . "reports/viewSubscribed" ?>"><i
											class="fa fa-pie-chart fa-5x bg-color-lavetafebbraio"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-lavetafebbraio">
									<a class="white" href="<?php echo base_url() . "reports/viewSubscribed"
									?>">Shares Subscribed</a>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-Sun-RipenedBikini">
								<div class="panel-body">
									<a href="<?php echo base_url() . "reports/payments_over_time" ?>"><i
											class="fa fa-money fa-5x bg-color-Sun-RipenedBikini"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-Sun-RipenedBikini">
									<a class="white" href="<?php echo base_url() . "reports/payments_over_time"
									?>">Shareholders Share Payments</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="agentsReports" class="tab-pane fade">
					<hr/>
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-wisteria">
								<div class="panel-body">
									<a href="<?php echo base_url() . "reports/viewAllAgents" ?>"><i
											class="fa fa-user-plus fa-5x bg-color-wisteria"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-wisteria">
									<a class="white" href="<?php echo base_url() . "reports/viewAllAgents"
									?>">All Agents Report</a>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-green">
								<div class="panel-body">
									<a href="<?php echo base_url() . "reports/agentsView" ?>"><i
											class="fa fa-users fa-5x bg-color-green"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-green">
									<a class="white" href="<?php echo base_url() . "reports/agentsView"
									?>">Agents Shareholder Addition Over Time</a>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-lavetafebbraio">
								<div class="panel-body">
									<a href="<?php echo base_url() . "reports/agentsAddedOverTime" ?>"><i
											class="fa fa-pie-chart fa-5x bg-color-lavetafebbraio"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-lavetafebbraio">
									<a class="white" href="<?php echo base_url() . "reports/agentsAddedOverTime"
									?>">Agents Added Over Time</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="finanicialReports" class="tab-pane fade">
					<hr/>
					<?php
					if (($this->session->userdata('role') === 'Admin')) {
						?>
						<div class="row">
							<div class="col-md-3 col-sm-12 col-xs-12">
								<div class="panel panel-primary text-center no-boder bg-color-blue">
									<div class="panel-body">
										<a href="<?php echo base_url() . 'settings/addSystemInfo' ?>"><i
												class="fa fa-info-circle fa-5x bg-color-blue"></i></a>

										<h3></h3>
									</div>
									<div class="panel-footer back-footer-blue">
										<a class="white"
										   href="<?php echo base_url() . "settings/addSystemInfo" ?>">Add
											System Information</a>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>

