<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Shareholders extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('shareholders/shareholders_model', 'shareholders');
		$this->load->model('settings/settings_model', 'settings');
	}

	public function viewShareholders($sort_by = 'Name', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
			OR ($this->session->userdata('role') == "Admin2")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");

				$limit = 15;
				$data['unapproved'] =
					array("physicalFormNumber" => "CB Form Number", "Name" => "Full Names", "IdNo" => "Id Number",
						"Telephone" => "Mobile Number", "Email" => "Email", "Box" => "Box Number",
						"Country" => "Country", "Village" => "Village");
				$data['approved'] = array("physicalFormNumber" => "CB Form Number", "formnumber" => "Form Number",
					"Name" => "Full Names", "IdNo" => "Id Number", "Telephone" => "Mobile Number", "Email" => "Email",
					"Box" => "Box Number", "Country" => "Country");
				$data['headers'] =
					array("physicalFormNumber" => "CB Form Number", "Name" => "Full Names", "IdNo" => "Id Number",
						"Telephone" => "Mobile Number", "Email" => "Email", "Box" => "Box Number",
						"Country" => "Country");
				$result = $this->shareholders->get($limit, $offset, $sort_by, $sort_order);
				$index = null;
				$result2 = $this->shareholders->getIndividualSearch($index);
				$data['view_data2'] = $result2['rows'];
				$data['mainContent'] = 'viewShareholders';
				$data['title'] = "View ShareHolders";
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];

				$data['view_data1'] = $result['rows1'];
				$data['rownumber1'] = $result['row_count1'];
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("shareholders/viewShareholders/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				$data['info'] = ("Share Holders Loaded Successfully!");
				$this->load->view('Admin/adminRedirect', $data);
				$this->load->view('Admin/includes/datatables');
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function viewCompanyShareholders($sort_by = 'physicalFormNumber', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
			OR ($this->session->userdata('role') == "Admin2")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");

				$limit = 15;
				$data['unapproved'] = array("physicalFormNumber" => "CB Form Number", "Name" => "Full Names",
					"cin" => "Certificate Of Incorporation", "krapin" => "KRA Pin Number",
					"Telephone" => "Mobile Number", "Email" => "Email", "Box" => "Box Number", "Country" => "Country");

				$data['approved'] = array("physicalFormNumber" => "CB Form Number", "Name" => "Full Names",
					"cin" => "Certificate Of Incorporation", "krapin" => "KRA Pin Number",
					"Telephone" => "Mobile Number", "Email" => "Email", "Box" => "Box Number", "Country" => "Country");
				$data['headers'] = array("physicalFormNumber" => "CB Form Number", "Name" => "Full Names",
					"cin" => "Certificate Of Incorporation", "krapin" => "KRA Pin Number",
					"Telephone" => "Mobile Number", "Email" => "Email", "Box" => "Box Number", "Country" => "Country");
				$result = $this->shareholders->getCompany($limit, $offset, $sort_by, $sort_order);
				$index = null;
				$result2 = $this->shareholders->getCompanySearch($index);
				$data['view_data2'] = $result2['rows'];
				$data['mainContent'] = 'viewCompanyRegistration';
				$data['title'] = "View Company Shareholding";
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];

				$data['view_data1'] = $result['rows1'];
				$data['rownumber1'] = $result['row_count1'];
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("shareholders/viewCompanyShareholders/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				$data['info'] = ("Company Shareholders Loaded Successfully!");
				$this->load->view('Admin/adminRedirect', $data);
				$this->load->view('Admin/includes/datatables');
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function viewNextofkin($sort_by = 'physicalFormNumber', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");

				$limit = 15;
				$data['fields'] = array("physicalFormNumber" => "CB Form Number", "kinName" => "Full Names",
					"kinIdentity" => "Kin Identity", "kinPhonenumber" => "Phonenumber", "percentage" => "Percentage",
					"KintoName" => "Shareholder Name", "kintoidnumber" => "Shareholder Id Number");
				$data['headers'] = array("physicalFormNumber" => "CB Form Number", "kinName" => "Full Names",
					"kinIdentity" => "Kin Identity", "kinPhonenumber" => "Phonenumber", "percentage" => "Percentage",
					"KintoName" => "Shareholder Name", "kintoidnumber" => "Shareholder Id Number");

				$result = $this->shareholders->getKin($limit, $offset, $sort_by, $sort_order);
				$index = null;
				$result2 = $this->shareholders->getkinsearch($index);
				$data['view_data2'] = $result2['rows'];
				$data['mainContent'] = 'viewNextofKin';
				$data['title'] = "View Next of Kin";
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("shareholders/viewNextofKin/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				$data['info'] = ("Next of kin was Loaded Successfully!");
				$this->load->view('Admin/adminRedirect', $data);
				$this->load->view('Admin/includes/datatables');
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function searchNextofkin($sort_by = 'physicalFormNumber', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");

				$limit = 15;
				$data['fields'] = array("physicalFormNumber" => "CB Form Number", "kinName" => "Full Names",
					"kinIdentity" => "Kin Identity", "kinPhonenumber" => "Phonenumber", "percentage" => "Percentage",
					"KintoName" => "Shareholder Name", "kintoidnumber" => "Shareholder Id Number");
				$data['headers'] = array("physicalFormNumber" => "CB Form Number", "kinName" => "Full Names",
					"kinIdentity" => "Kin Identity", "kinPhonenumber" => "Phonenumber", "percentage" => "Percentage",
					"KintoName" => "Shareholder Name", "kintoidnumber" => "Shareholder Id Number");

				$result = $this->shareholders->getKin($limit, $offset, $sort_by, $sort_order);
				$index = $this->input->post('search');
				$result2 = $this->shareholders->getkinsearch($index);
				$data['view_data2'] = $result2['rows'];
				$data['mainContent'] = 'viewNextofKin';
				$data['title'] = "View Next of Kin";
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];
				if ($result2['rows'] == null) {
					$data['error'] = ("No Next of kin for the specified shareholder was found");
				} else {
					$data['success'] = ("Next of kin Loaded Successfully!");
				}
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("shareholders/searchNextofkin/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				$this->load->view('Admin/adminRedirect', $data);
				$this->load->view('Admin/includes/datatables');
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function searchIndividual($sort_by = 'Name', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");

				$limit = 15;
				$data['unapproved'] = array("Name" => "Full Names", "IdNo" => "Id Number", "Telephone" => "Mobile Number",
					"Email" => "Email", "Box" => "Box Number", "Town" => "Town", "County" => "County",
					"Country" => "Country", "Village" => "Village");
				$data['approved'] = array("formnumber" => "Form Number", "Name" => "Full Names", "IdNo" => "Id Number",
					"Telephone" => "Mobile Number", "Email" => "Email", "Box" => "Box Number", "Town" => "Town",
					"County" => "County", "Country" => "Country", "Village" => "Village");
				$data['headers'] = array("Name" => "Full Names", "IdNo" => "Id Number", "Telephone" => "Mobile Number",
					"Email" => "Email", "Box" => "Box Number", "Town" => "Town", "County" => "County",
					"Country" => "Country", "Village" => "Village");
				$result = $this->shareholders->get($limit, $offset, $sort_by, $sort_order);
				$index = $this->input->post('search');
				$result2 = $this->shareholders->getIndividualSearch($index);
				$data['view_data2'] = $result2['rows'];
				$data['mainContent'] = 'viewShareholders';
				$data['title'] = "View ShareHolders";
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];

				$data['view_data1'] = $result['rows1'];
				$data['rownumber1'] = $result['row_count1'];
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("shareholders/searchIndividual/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				if ($result2['rows'] == null) {
					$data['error'] = ("No Shareholder with specified KRA pin was found");
				} else {
					$data['success'] = ("Shareholder Loaded Successfully!");
				}
				$this->load->view('Admin/adminRedirect', $data);
				$this->load->view('Admin/includes/datatables');
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function searchCompany($sort_by = 'Name', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");

				$limit = 15;
				$data['unapproved'] = array("Name" => "Full Names", "cin" => "Certificate Of Incorporation",
					"krapin" => "KRA Pin Number", "Telephone" => "Mobile Number", "Email" => "Email",
					"Box" => "Box Number", "Country" => "Country", "County" => "County", "Town" => "Town",
					"Village" => "Village");

				$data['approved'] = array("Name" => "Full Names", "cin" => "Certificate Of Incorporation",
					"krapin" => "KRA Pin Number", "Telephone" => "Mobile Number", "Email" => "Email",
					"Box" => "Box Number", "Country" => "Country", "Town" => "Town");
				$data['headers'] = array("Name" => "Full Names", "cin" => "Certificate Of Incorporation",
					"krapin" => "KRA Pin Number", "Telephone" => "Mobile Number", "Email" => "Email",
					"Box" => "Box Number", "Country" => "Country", "County" => "County", "Town" => "Town",
					"Village" => "Village");
				$result = $this->shareholders->getCompany($limit, $offset, $sort_by, $sort_order);
				$index = $this->input->post('search');
				$result2 = $this->shareholders->getCompanySearch($index);
				$data['view_data2'] = $result2['rows'];
				$data['mainContent'] = 'viewCompanyRegistration';
				$data['title'] = "View Company Shareholding";
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];

				$data['view_data1'] = $result['rows1'];
				$data['rownumber1'] = $result['row_count1'];
				if ($result2['rows'] == null) {
					$data['error'] = ("No Shareholder with specified KRA pin was found");
				} else {
					$data['success'] = ("Shareholder Loaded Successfully!");
				}
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("shareholders/getCompanySearch/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				$this->load->view('Admin/adminRedirect', $data);
				$this->load->view('Admin/includes/datatables');
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function addShareholders()
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
			OR ($this->session->userdata('role') == "Admin2") OR ($this->session->userdata('role') == "Agent")
		) {
			if ($this->session->userdata('logged_in')) {
				$result = $this->shareholders->get_agentsdropdown();
				$index['view_data'] = $result['rows'];
				$index['mainContent'] = 'addShareholder';
				$index['success'] = ("");
				$index['error'] = ("");
				$index['info'] = ("");
				$index['title'] = "Add Individual Shareholders";

				$this->load->view('Admin/adminRedirect', $index);
			} else {
				redirect('main/restricted');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function addCompany()
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
			OR ($this->session->userdata('role') == "Admin2") OR ($this->session->userdata('role') == "Agent")
		) {
			if ($this->session->userdata('logged_in')) {
				$result = $this->shareholders->get_agentsdropdown();
				$index['view_data'] = $result['rows'];
				$index['success'] = ("");
				$index['error'] = ("");
				$index['info'] = ("");
				$index['mainContent'] = 'companyRegistration';
				$index['title'] = "Add Company Shareholders";

				$this->load->view('Admin/adminRedirect', $index);
			} else {
				redirect('main/restricted');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function shareholderAdd()
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
			OR ($this->session->userdata('role') == "Admin2") OR ($this->session->userdata('role') == "Agent")
		) {
			if ($this->session->userdata('logged_in')) {
				$index['mainContent'] = 'shareHolderHome';
				$index['success'] = ("");
				$index['error'] = ("");
				$index['info'] = ("");
				$index['title'] = "Shareholders Registrations";

				$this->load->view('Admin/adminRedirect', $index);
			} else {
				redirect('main/restricted');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function modify($id = null)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit found', 500);
				} else {
					$result = $this->shareholders->edit($id);
					$index['view_data'] = $result;
					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$index['mainContent'] = 'editShareholder';
					$index['title'] = "Edit Shareholders";
					$index['view_data'] = $result;
					/*	$index['agent'] = $result['agent'];*/

					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function modifyCompany($id = null)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit found', 500);
				} else {
					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$result = $this->shareholders->editCompany($id);
					$index['view_data'] = $result;
					$index['mainContent'] = 'editCompanyRegistration';
					$index['title'] = "Edit Company Shareholders";
					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function approve($id = null)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit found', 500);
				} else {
					$result = $this->shareholders->edit($id);
					$index['view_data'] = $result;
					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$index['mainContent'] = 'approveShareholder';
					$index['title'] = "Approve Shareholders";
					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function approveCompany($id = null)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit found', 500);
				} else {
					$result = $this->shareholders->editCompany($id);
					$index['view_data'] = $result;
					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$index['mainContent'] = 'approveShareholder';
					$index['title'] = "Approve Shareholders";
					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function kinmodify($id = null)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit found', 500);
				} else {

					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$index['mainContent'] = 'editNextofKin';
					$index['title'] = "Edit Next of Kin";
					$result = $this->shareholders->kinedit($id);
					$index['view_data'] = $result;

					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function printRegistration($id = null)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")
			OR ($this->session->userdata('role') == "Agent") OR ($this->session->userdata('role') == "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit found', 500);
				} else {
					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$index['mainContent'] = 'printRegistration';
					$index['title'] = "Print Shareholder";
					$index['view_data'] = $this->shareholders->printout($id);
					$index['view_data1'] = $this->settings->editSystemInfo();
					$index['view_data2'] = $this->shareholders->getAgent($id);
					$payment = $this->shareholders->getpayments($id);
					$index['view_data4'] = $payment['rows'];
					$reg = $this->shareholders->getreg($id);
					$index['view_data5'] = $reg['rows'];
					$shares = $this->shareholders->getshares($id);
					$index['view_data6'] = $shares['rows'];
					$request = $this->shareholders->nextofkin($id);
					$index['kins'] = $request['rows'];

					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function addNextofkin()
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$idnumber = 0;
				$result = $this->shareholders->get_search2($idnumber);
				$data['mainContent'] = 'addNextofkin';
				$data['title'] = "Add Next Of Kins";
				$data['view_data'] = $result;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function buyShares()
	{
		if (($this->session->userdata('role') == 'Admin') OR ($this->session->userdata('role') == 'Admin2')
			OR ($this->session->userdata('role') == 'Finance') OR ($this->session->userdata('role') == 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$idnumber = 0;
				$result = $this->shareholders->get_search($idnumber);
				$data['mainContent'] = 'buyShares';
				$data['title'] = "Buy Shares";
				$data['view_data'] = $result;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function remove($id = null)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit provided', 500);
				} else {
					$this->shareholders->remove($id);
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['success'] = ("Share holder deleted successfully");
					header('Location:' . base_url('shareholders/viewShareholders', $data));
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function removeCompany($id = null)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit provided', 500);
				} else {
					$this->shareholders->remove($id);
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['success'] = ("Share holder deleted successfully");
					header('Location:' . base_url('shareholders/viewCompanyShareholders', $data));
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function kinremove($id = null)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit provided', 500);
				} else {
					$this->shareholders->kinremove($id);
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['success'] = ("Next of Kin deleted successfully");
					header('Location:' . base_url('shareholders/viewNextofkin', $data));
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function save()
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")
			OR ($this->session->userdata('role') == "Agent") OR ($this->session->userdata('role') == "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$type = $this->input->post('type');
				$this->form_validation->set_rules('name', 'Enter Shareholder names', 'required|trim');
				if ($type == 0) {
					$this->form_validation->set_rules('idnumber', 'Id / Passport Number', 'required|trim');
				} else {
					$this->form_validation->set_rules('cert', 'Certificate of Incorpoaration', 'required|trim');
					$this->form_validation->set_rules('kra', 'KRA pin Number', 'required|trim');
				}
				$this->form_validation->set_rules('country', 'Country', 'required|trim');
				$this->form_validation->set_rules('county', 'County', 'required|trim');
				$this->form_validation->set_rules('town', 'Town', 'required|trim');
				$this->form_validation->set_rules('village', 'Village', 'required|trim');
				$this->form_validation->set_rules('fnumber', 'Physical Form Number', 'required|trim');
				$this->form_validation->set_rules('mobile', 'Mobile number', 'required|trim|min_length[10]');
				$this->form_validation->set_message('is_unique', "This email has already been taken");
				$agent = $this->input->post('agents');
				$username = $this->input->post('username');
				$exist = $this->shareholders->getusername($username);
				if ($agent != 0) {
					if ($this->form_validation->run()) {
						if ($exist != $username) {
							if (isset($_POST) && $_POST['save'] == 'Add Shareholder') {
								if ($type == 0) {
									$index['Name'] = $this->input->post('name');
									$index['type'] = $type;
									$index['IdNo'] = $this->input->post('idnumber');
									$index['Box'] = $this->input->post('postaladdress');
									$index['Email'] = $this->input->post('email');
									$index['Village'] = $this->input->post('village');
									$index['Town'] = $this->input->post('town');
									$index['County'] = $this->input->post('county');
									$index['Country'] = $this->input->post('country');
									$index['Telephone'] = $this->input->post('mobile');
									$index['physicalFormNumber'] = $this->input->post('fnumber');
									$index['agentID'] = $this->input->post('agents');
									$index['AddedBy'] = $this->input->post('addedby');
									$index['dateadded'] = date("Y-m-d");
									$this->shareholders->add($index);
									$user['username'] = $this->input->post('idnumber');
									$user['password'] = md5("password");
									$user['role'] = $this->input->post('role');
									$user['link3'] = $this->db->insert_id();;
									$this->shareholders->addUser($user);
								} else {
									$index['Name'] = $this->input->post('name');
									$index['type'] = $type;
									$index['cin'] = $this->input->post('cert');
									$index['krapin'] = $this->input->post('kra');
									$index['Box'] = $this->input->post('postaladdress');
									$index['Email'] = $this->input->post('email');
									$index['Village'] = $this->input->post('village');
									$index['Town'] = $this->input->post('town');
									$index['County'] = $this->input->post('county');
									$index['Country'] = $this->input->post('country');
									$index['Telephone'] = $this->input->post('mobile');
									$index['physicalFormNumber'] = $this->input->post('fnumber');
									$index['agentID'] = $this->input->post('agents');
									$index['AddedBy'] = $this->input->post('addedby');
									$index['dateadded'] = date("Y-m-d");
									$this->shareholders->add($index);
									$user['username'] = $this->input->post('kra');
									$user['password'] = md5("password");
									$user['role'] = $this->input->post('role');
									$user['link3'] = $this->db->insert_id();;
									$this->shareholders->addUser($user);
								}
								$data['success'] = ("");
								$data['error'] = ("");
								$data['info'] = ("");
								if ($type == 0) {
									$data['mainContent'] = 'addShareholder';
									$data['title'] = "Add Individual Shareholders";
									$data['success'] = ("Shareholder was added successfully");
								} else {
									$data['mainContent'] = 'companyRegistration';
									$data['title'] = "Add Company Shareholders";
									$data['success'] = ("Company Share holder was added successfully");
								}
								$result = $this->shareholders->get_agentsdropdown();
								$data['view_data'] = $result['rows'];
								$this->load->view('Admin/adminRedirect', $data);
							} else {
								$data['success'] = ("");
								$data['error'] = ("");
								$data['info'] = ("");
								if ($type == 0) {
									$data['mainContent'] = 'addShareholder';
									$data['title'] = "Add Individual Shareholders";
									$data['error'] = ("Shareholder Could not be registered");
								} else {
									$data['mainContent'] = 'companyRegistration';
									$data['title'] = "Add Company Shareholders";
									$data['error'] = ("Company Shareholder Could not be registered");
								}
								$result = $this->shareholders->get_agentsdropdown();
								$data['view_data'] = $result['rows'];
								$this->load->view('Admin/adminRedirect', $data);
							}
						} else {
							$data['success'] = ("");
							$data['error'] = ("");
							$data['info'] = ("");
							if ($type == 0) {
								$data['mainContent'] = 'addShareholder';
								$data['title'] = "Add Individual Shareholders";
								$data['error'] = ("The choosen username is already taken. Please choose a new one");
							} else {
								$data['mainContent'] = 'companyRegistration';
								$data['title'] = "Add Company Shareholders";
								$data['error'] = ("The choosen username is already taken. Please choose a new one");
							}
							$result = $this->shareholders->get_agentsdropdown();
							$data['view_data'] = $result['rows'];
							$this->load->view('Admin/adminRedirect', $data);
						}
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						if ($type == 0) {
							$data['mainContent'] = 'addShareholder';
							$data['title'] = "Add Individual Shareholders";
						} else {
							$data['mainContent'] = 'companyRegistration';
							$data['title'] = "Add Company Shareholders";
						}
						$result = $this->shareholders->get_agentsdropdown();
						$data['view_data'] = $result['rows'];
						$data['error'] = ("Please fill all the spaces appropriately");

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					if ($type == 0) {
						$data['mainContent'] = 'addShareholder';
						$data['title'] = "Add Individual Shareholders";
					} else {
						$data['mainContent'] = 'companyRegistration';
						$data['title'] = "Add Company Shareholders";
					}
					$result = $this->shareholders->get_agentsdropdown();
					$data['view_data'] = $result['rows'];
					$data['error'] =
						("Please Choose an Agent. For Share Holder with no agent choose Churchblaze Group Limited");

					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function update($id = null)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")
			OR ($this->session->userdata('role') == "Agent") OR ($this->session->userdata('role') == "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$type = $this->input->post('type');
				if ($type == 0) {
					$this->form_validation->set_rules('idnumber', 'Id / Passport Number', 'required|trim');
				} else {
					$this->form_validation->set_rules('cert', 'Certificate of Incorpoaration', 'required|trim');
					$this->form_validation->set_rules('kra', 'KRA pin Number', 'required|trim');
				}
				$this->form_validation->set_rules('name', 'Enter Shareholder names', 'required|trim');
				$this->form_validation->set_rules('postaladdress', 'Postal Address', 'required|trim');
				$this->form_validation->set_rules('country', 'Country', 'required|trim');
				$this->form_validation->set_rules('county', 'County', 'required|trim');
				$this->form_validation->set_rules('town', 'Town', 'required|trim');
				$this->form_validation->set_rules('village', 'Village', 'required|trim');
				$this->form_validation->set_rules('fnumber', 'Physical Form Number', 'required|trim');
				$this->form_validation->set_rules('mobile', 'Mobile number', 'required|trim|min_length[10]');
				$this->form_validation->set_rules('username', 'Username', 'required|trim');
				$this->form_validation->set_message('is_unique', "This email has already been taken");
				$agent = $this->input->post('agentID');
				$username = $this->input->post('username');
				$exist = $this->shareholders->getusername($username);
				if ($this->form_validation->run()) {
					if (isset($_POST) && $_POST['update'] == 'Update Shareholder') {
						if ($type == 0) {
							$index['Id'] = $this->input->post('Id');
							$index['Name'] = $this->input->post('name');
							$index['IdNo'] = $this->input->post('idnumber');
							$index['Box'] = $this->input->post('postaladdress');
							$index['Email'] = $this->input->post('email');
							$index['Village'] = $this->input->post('village');
							$index['Town'] = $this->input->post('town');
							$index['County'] = $this->input->post('county');
							$index['Country'] = $this->input->post('country');
							$index['Telephone'] = $this->input->post('mobile');
							$index['physicalFormNumber'] = $this->input->post('fnumber');
							$index['agentID'] = $this->input->post('agentID');
							$index['AddedBy'] = $this->input->post('addedby');
							$index['dateadded'] = $this->input->post('dateadded');
							$index['UpdatedBy'] = $this->input->post('editedby');
							$index['dateupdated'] = date("Y-m-d");
							$this->shareholders->add($index);
							$user['id'] = $this->input->post('id');
							$user['username'] = $this->input->post('idnumber');
							$user['password'] = md5("password");
							$user['role'] = $this->input->post('role');
							$user['link3'] = $this->input->post('Id');
							$this->shareholders->addUser($user);
						} else {
							$index['Id'] = $this->input->post('Id');
							$index['Name'] = $this->input->post('name');
							$index['cin'] = $this->input->post('cert');
							$index['krapin'] = $this->input->post('kra');
							$index['Box'] = $this->input->post('postaladdress');
							$index['Email'] = $this->input->post('email');
							$index['Village'] = $this->input->post('village');
							$index['Town'] = $this->input->post('town');
							$index['County'] = $this->input->post('county');
							$index['Country'] = $this->input->post('country');
							$index['Telephone'] = $this->input->post('mobile');
							$index['physicalFormNumber'] = $this->input->post('fnumber');
							$index['AddedBy'] = $this->input->post('addedby');
							$index['dateadded'] = $this->input->post('dateadded');
							$index['UpdatedBy'] = $this->input->post('editedby');
							$index['dateupdated'] = date("Y-m-d");
							$this->shareholders->add($index);
							$user['id'] = $this->input->post('id');
							$user['username'] = $this->input->post('kra');
							$user['password'] = md5("password");
							$user['role'] = $this->input->post('role');
							$user['link3'] = $this->input->post('Id');
							$this->shareholders->addUser($user);
						}
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						if ($type == 0) {
							$data['mainContent'] = 'editShareholder';
							$data['title'] = "Edit Shareholders";
							$result = $this->shareholders->edit($this->input->post('Id'));
							$data['view_data'] = $result;
							$data['success'] = ("Shareholder was edited successfully");
						} else {
							$result = $this->shareholders->editCompany($this->input->post('Id'));
							$data['view_data'] = $result;
							$data['mainContent'] = 'editCompanyRegistration';
							$data['title'] = "Edit Company Shareholders";
							$data['success'] = ("Company Shareholder was edited successfully");
						}

						$this->load->view('Admin/adminRedirect', $data);
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						if ($type == 0) {
							$data['mainContent'] = 'editShareholder';
							$data['title'] = "Edit Shareholders";
							$result = $this->shareholders->edit($this->input->post('Id'));
							$data['view_data'] = $result;
							$data['error'] = ("Shareholder could not edited");
						} else {
							$result = $this->shareholders->editCompany($this->input->post('Id'));
							$data['view_data'] = $result;
							$data['mainContent'] = 'editCompanyRegistration';
							$data['title'] = "Edit Company Shareholders";
							$data['error'] = ("Company Shareholder could not edited");
						}
						$this->load->view('Admin/adminRedirect', $data);
					}

				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					if ($type == 0) {
						$data['mainContent'] = 'editShareholder';
						$data['title'] = "Edit Shareholders";
						$result = $this->shareholders->edit($this->input->post('Id'));
						$data['view_data'] = $result;
					} else {
						$result = $this->shareholders->editCompany($this->input->post('Id'));
						$data['view_data'] = $result;
						$data['mainContent'] = 'editCompanyRegistration';
						$data['title'] = "Edit Company Shareholders";
					}
					$data['error'] = ("Please fill all the spaces appropriately");

					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function approvesave($id = null)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				$type = $this->input->post('type');
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$approve = $this->input->post('approval');
				$form = $this->shareholders->formNumber();
				if (isset($_POST) && $_POST['approve'] == 'Approve Shareholder') {
					$index['Id'] = $this->input->post('Id');
					$index['Approval'] = $approve;
					if ($approve == 1) {
						$index['formnumber'] = $form;
						$index['activatedBy'] = $this->session->userdata('name');
						$index['dateActivated'] = date("Y-m-d");
					} else {
						$index['formnumber'] = "none";
						$index['deactivatedBy'] = $this->session->userdata('name');
						$index['dateDeactivated'] = date("Y-m-d");
					}
					$this->shareholders->add($index);
					log_message('info', 'Shareholder was approved successfully by:-' . $this->session->userdata('name'));
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					if ($type == 0) {
						$data['success'] = ("Shareholder was approved successfully");
						header('Location:' . base_url('shareholders/viewShareholders', $data));
					} else {
						$data['success'] = ("Shareholder was approved successfully");
						header('Location:' . base_url('shareholders/viewCompanyShareholders', $data));
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					if ($type == 0) {
						$data['mainContent'] = 'approveShareholder';
						$data['title'] = "Approve Shareholders";
						$result = $this->shareholders->edit($this->input->post('Id'));
						$data['view_data'] = $result;
					} else {
						$result = $this->shareholders->editCompany($this->input->post('Id'));
						$data['view_data'] = $result;
						$data['mainContent'] = 'approveShareholder';
						$data['title'] = "Approve Shareholders";
					}
					$data['error'] = ("Approval Was not successful");
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function search($sort_by = 'Id', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
			OR ($this->session->userdata('role') == "Agent")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$limit = 15;
				$data['fields'] = array("Id" => "Id", "Name" => "Names", "IdNo" => "Id Number", "Telephone" => "Telephone",
					"Email" => "Email");

				$index = $this->input->post('search');
				$result = $this->shareholders->get_search($index, $limit, $offset, $sort_by, $sort_order);
				$data['mainContent'] = 'buyShares';
				$data['title'] = "Buy Shares";
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];

				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("shareholders/buyShares/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;

				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function search2()
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$idnumber = $this->input->post('search');
				$result = $this->shareholders->get_search2($idnumber);;
				$data['mainContent'] = 'addNextofkin';
				$data['title'] = "Add Next Of Kins";
				$data['view_data'] = $result;
				if ($result == null) {
					$data['error'] =
						("No Share holder with the specified Id Number was found. Make sure that the user us Approved");
				} else {
					$data['success'] = ("Share holder was retrieved successfully");
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function kinsave()
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('names', 'Names must be entered', 'required|trim');
				$this->form_validation->set_rules('identity', 'Id / Passport Number', 'required|trim');
				$this->form_validation->set_rules('relation', 'Country', 'required|trim');
				$this->form_validation->set_rules('percentage', 'County', 'required|trim');
				$this->form_validation->set_rules('fnumber', 'Physical Form Number', 'required|trim');
				$email = $this->input->post('email');
				$phone = $this->input->post('mobile');
				if ($this->form_validation->run()) {
					if (isset($_POST) && $_POST['add'] == 'Add Next Of Kin') {
						$index['kinName'] = $this->input->post('names');
						$index['kinIdentity'] = $this->input->post('identity');
						if ($email == null) {
							$index['email'] = "N/A";
						} else {
							$index['email'] = $this->input->post('email');
						}
						if ($phone == null) {
							$index['kinPhonenumber'] = "N/A";
						} else {
							$index['kinPhonenumber'] = $this->input->post('mobile');
						}
						$index['percentage'] = $this->input->post('percentage');
						$index['relation'] = $this->input->post('relation');
						$index['kinTo'] = $this->input->post('Id');
						$index['KintoName'] = $this->input->post('holder');
						$index['physicalFormNumber'] = $this->input->post('fnumber');
						$index['kintoidnumber'] = $this->input->post('kinidnumber');
						$index['AddedBy'] = $this->input->post('addedby');
						$index['dateadded'] = date("Y-m-d");
						$this->shareholders->addKin($index);
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$index = 0;
						$result = $this->shareholders->get_search($index);
						$data['mainContent'] = 'addNextofkin';
						$data['title'] = "Add Next Of Kins";
						$data['view_data'] = $result;
						$data['success'] = ("Next of Kin was added successfully");
						$this->load->view('Admin/adminRedirect', $data);
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$index = 0;
						$result = $this->shareholders->get_search($index);
						$data['mainContent'] = 'addNextofkin';
						$data['title'] = "Add Next Of Kins";
						$data['view_data'] = $result;
						$data['error'] = ("Next of Kin Could not be registered");

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$index = 0;
					$result = $this->shareholders->get_search($index);
					$data['mainContent'] = 'addNextofkin';
					$data['title'] = "Add Next Of Kins";
					$data['view_data'] = $result;
					$data['error'] = ("Please fill all the spaces appropriately");

					$this->load->view('Admin/adminRedirect', $data);
				}

			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function kinupdate($id = null)
	{
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('names', 'Names must be entered', 'required|trim');
				$this->form_validation->set_rules('identity', 'Id / Passport Number', 'required|trim');
				$this->form_validation->set_rules('relation', 'Country', 'required|trim');
				$this->form_validation->set_rules('percentage', 'County', 'required|trim');
				$this->form_validation->set_rules('fnumber', 'Physical Form Number', 'required|trim');
				$email = $this->input->post('email');
				$phone = $this->input->post('mobile');
				if ($this->form_validation->run()) {
					if (isset($_POST) && $_POST['edit'] == 'Update Next Of Kin') {
						$index['kinId'] = $this->input->post('kinId');
						$index['kinName'] = $this->input->post('names');
						$index['kinIdentity'] = $this->input->post('identity');
						if ($email == null) {
							$index['email'] = "N/A";
						} else {
							$index['email'] = $this->input->post('email');
						}
						if ($phone == null) {
							$index['kinPhonenumber'] = "N/A";
						} else {
							$index['kinPhonenumber'] = $this->input->post('mobile');
						}
						$index['percentage'] = $this->input->post('percentage');
						$index['relation'] = $this->input->post('relation');
						$index['kinTo'] = $this->input->post('kinTo');
						$index['KintoName'] = $this->input->post('holder');
						$index['kintoidnumber'] = $this->input->post('kinidnumber');
						$index['physicalFormNumber'] = $this->input->post('fnumber');
						$index['AddedBy'] = $this->input->post('addedby');
						$index['dateadded'] = $this->input->post('dateadded');
						$index['UpdatedBy'] = $this->input->post('editedby');
						$index['dateupdated'] = date("Y-m-d");
						$this->shareholders->addKin($index);
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'editNextofKin';
						$data['title'] = "Edit Next Of Kins";
						$result = $this->shareholders->kinedit($this->input->post('kinId'));
						$data['view_data'] = $result;
						$data['success'] = ("Next of Kin was updated successfully");
						$this->load->view('Admin/adminRedirect', $data);
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'addNextofkin';
						$data['title'] = "Add Next Of Kins";
						$result = $this->shareholders->kinedit($this->input->post('kinId'));
						$data['view_data'] = $result;
						$data['error'] = ("Next of Kin Could not be Updated");

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['mainContent'] = 'editNextofKin';
					$data['title'] = "Edit Next Of Kins";
					$result = $this->shareholders->kinedit($this->input->post('kinId'));
					$data['view_data'] = $result;
					$data['error'] = ("Please fill all the spaces appropriately");

					$this->load->view('Admin/adminRedirect', $data);
				}

			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function shareholder_details($id = null)
	{
		$role = $this->session->userdata('role');
		if ($role == 'Admin' || $role == 'Admin2' || $role == 'Finance' || $role = 'Agent') {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shareholder with specified user Id was found', 500);
				} else {
					$data['success'] = '';
					$data['info'] = '';
					$data['error'] = '';
					$data['nok'] = array("kinName" => "Kin Name", "kinIdentity" => "Kin Identity",
						"kinPhonenumber" => "Phonenumber", "email" => "Email", 'percentage' => 'Share %',
						"physicalFormNumber" => "CB Shareholder Form #");
					$data['payment'] = array("PaymentUUID" => "System Payment Id", 'physicalReceiptNumber' => 'CB Receipt #',
						"type" => "Series", "Amount" => "Amount Paid", "numberofshares" => "# of shares",
						'PaymentType' => 'Payment Type', "Code" => "Payment Code",
						'Receiptnumber' => 'System Subscription Code');
					$data['share'] = array('physicalFormNumber' => 'CB Shareholder #', "Receiptnumber" =>
						"System Receipt #", "type" => "Series", "shareNumber" => "# of Shares",
						"expectedAmount" => 'Expected Amount', 'datebought' => 'Date Subscribed');
					$data['mainContent'] = 'viewDetails';
					$data['title'] = 'Shareholder Details';
					$shareholder = $this->shareholders->getShareholder($id);
					$nok = $this->shareholders->shareholderKinDetails($id);
					$payment = $this->shareholders->shareholderKinPayments($id);
					$share = $this->shareholders->shareholderkinShareSubscription($id);
					$data['kin'] = $nok['rows'];
					$data['payments'] = $payment['rows'];
					$data['shares'] = $share['rows'];
					$data['shareholders'] = $shareholder;
					$data['type'] = $shareholder['type'];
					if (($payment['rows'] == null || $nok['rows'] == null || $share['rows'] == null || $shareholder ==
							null) && $shareholder['type'] == 0
					) {
						$data['info'] = 'Some of the shareholder details were empty';
					} elseif (($payment['rows'] == null || $share['rows'] == null || $shareholder ==
							null) && $shareholder['type'] == 1
					) {
						$data['info'] = 'Some of the shareholder details were empty';
					} else {
						$data['success'] = 'All details we retrieved successfully';
					}
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function upload_Avatar()
	{
		$role = $this->session->userdata('role');
		if ($role == 'Admin' || $role == 'Admin2' || $role == 'Finance' || $role == 'Agent') {
			if ($this->session->userdata('logged_in')) {
				
			} else {
				redirect('systemUsers/accessDeniec');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
}
