<div id="page-wrapper">
	<!--BEGIN TITLE & BREADCRUMB PAGE-->
	<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
		<div class="page-header pull-left">
			<div class="page-title">
				Shareholder Details
			</div>
		</div>
		<ol class="breadcrumb page-breadcrumb pull-right">
			<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
			</li>
			<li><i class="fa fa-users"></i><a href='<?php echo base_url() . 'shareholders/shareholderAdd' ?>'>&nbsp;&nbsp;Shareholder
					Management</a></li>
			<li class="active"><i class="fa fa-user-plus"></i>&nbsp;&nbsp;Shareholder Details</li>
		</ol>
		<div class="clearfix">
		</div>
	</div>
	<!--END TITLE & BREADCRUMB PAGE-->
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-info">
				<i class="fa fa-info-circle"></i>
				<strong>Heads up!</strong>
				This page is to show the deatils of the shareholder in one central place!
			</div>
			<br/>
			<?php if (strlen($success) > 0) {
				?>
				<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success . ''; ?>
				</div>
				<?php
			}
			?>
			<?php if (strlen($error) > 0) {
				?>
				<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
				</div>
				<?php
			}
			?>
			<?php if (strlen($info) > 0) {
				?>
				<div class="alert alert-info" id="error"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $info . ''; ?>
				</div>
				<?php
			}
			?>
		</div>
	</div>
	<!--BEGIN CONTENT-->
	<div class="page-content">
		<div id="tab-general">
			<div class="row mbl">
				<div class="col-lg-12">
					<ul class="nav nav-tabs nav-justified">
						<li class="active">
							<a data-toggle="tab" href="#avatar"><h5><i class="fa fa-user-secret"></i>&nbsp;Change
									Shareholder avatar</h5></a>
						</li>
						<li>
							<a data-toggle="tab" href="#details"><h5><i class="fa fa-user"></i>&nbsp;
									Shareholder Details</h5></a></li>
						<li>
							<a data-toggle="tab" href="#shares"><h5><i class="fa fa-briefcase"></i>&nbsp;Shares
									Subscribed</h5></a>
						</li>
						<li><a data-toggle="tab" href="#payments"><h5><i class="fa fa-money"></i>&nbsp;Shares
									Payments</h5></a>
						</li>
						<?php if ($type == 0) { ?>
							<li><a data-toggle="tab" href="#nok"><h5><i class="fa fa-users"></i>&nbsp;Next of Kins
									</h5></a>
							</li>
						<?php } ?>
					</ul>
					<div class="tab-content">
						<div id="avatar" class="tab-pane fade in active">
							<hr/>
							<div class="col-md-12">
								<!-- Advanced Tables -->
								<?php if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role')
									=== "Finance") OR ($this->session->userdata('role') === "Admin2")
								|| $this->session->userdata('role')) { ?>
								<div class="panel panel-pink">
									<?php } ?>
									<?php if (($this->session->userdata('role') === "Agent")) { ?>
									<div class="panel panel-green">
										<?php } ?>
										<div class="panel-heading">
											<div class="row">
												<div class="col-sm-6">
													<h3>Change or Add Share holder Avatars</h3>
												</div>
											</div>
										</div>
										<div class="panel-body">
											<?php $this->load->helper('form'); ?>
											<?php echo form_open_multipart('systemUsers/upload_Avatar'); ?>
											<div class="form-body pal">
												<div class="row">
													<div class="col-md-12">
														<div class="form-group">
															<label>Upload Shareholder Avatar</label>
															<?php echo form_upload(array("class" => "form-control",
																"placeholder" => "Enter the user avatar",
																"name" => "image", "required" => "true",
																'id' => 'image')) ?>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-4">
														<?php echo form_hidden('shareholderId', $shareholders['Id'],
															'class="form-control"'); ?>
													</div>
													<div class="col-md-4">
														<div class="form-group">
														</div>
													</div>
													<div class="col-md-4">
														<div class="form-group">
															<button type="reset" class="btn btn-danger pull-right">Cancel
															</button>
															<?php echo form_submit('save', 'Save Avatar',
																'class="btn btn-success pull-right margin-right"'); ?>
														</div>
													</div>
												</div>
											</div>
											</form>
										</div>
									</div>
								</div>
							</div>
							<div id="details" class="tab-pane fade">
								<hr/>
								<div class="col-md-12">
									<!-- Advanced Tables -->
									<?php if (($this->session->userdata('role') === "Admin")
									OR ($this->session->userdata('role') === "Finance") OR ($this->session->userdata('role')
										=== "Admin2")) { ?>
									<div class="panel panel-pink">
										<?php } ?>
										<?php if (($this->session->userdata('role') === "Agent")) { ?>
										<div class="panel panel-green">
											<?php } ?>
											<div class="panel-heading">
												<div class="row">
													<div class="col-sm-6">
														<h3>Shareholders Details</h3>
													</div>
												</div>
											</div>
											<div class="panel-body">
												<h4>Bio Data</h4>
												<hr>
												<div class="row">
													<div class="bio-row">
														<p><span class="bold"><i class="fa fa-forumbee">&nbsp;System Form
																	Number
																</i></span>:<span
																class="bold text-red">&nbsp;<?php echo $shareholders['formnumber'] ?></span>
														</p>
													</div>
													<div class="bio-row">
														<p><span class="bold"><i class="fa fa-forumbee">&nbsp;Churchblaze
																	Form Number
																</i></span>:<span
																class="bold text-red">&nbsp;<?php echo $shareholders['physicalFormNumber'] ?></span>
														</p>
													</div>
												</div>
												<hr>
												<div class="row">
													<div class="bio-row">
														<p><span class="fa fa-user"> Full Names </span>:&nbsp;<?php echo
															$shareholders['Name']
															?></p>
													</div>
													<?php if ($type == 0) { ?>
														<div class="bio-row">
															<p><span
																	class="fa fa-user-secret"> Id number </span>:&nbsp;<?php
																echo
																$shareholders['IdNo'] ?>
															</p>
														</div>
													<?php } ?>
													<?php if ($type == 1) { ?>
														<div class="bio-row">
															<p><span class="fa fa-certificate"> Certificate of Incorporation
																</span>:&nbsp;<?php
																echo
																$shareholders['cin'] ?>
															</p>
														</div>
														<div class="bio-row">
															<p><span class="fa fa-user-secret"> KRA Pin # </span>:&nbsp;
																<?php echo
																$shareholders['krapin'] ?>
															</p>
														</div>
													<?php } ?>
													<div class="bio-row">
														<p><span class="fa fa-map-marker"> Country</span>:&nbsp;<?php echo
															$shareholders['Country']
															?></p>
													</div>
													<div class="bio-row">
														<p><span class="fa fa-map-marker"> County </span>:&nbsp;<?php echo
															$shareholders['County']
															?></p>
													</div>
													<div class="bio-row">
														<p><span class="fa fa-location-arrow"> Town </span>:&nbsp;<?php echo
															$shareholders['Town']
															?></p>
													</div>
													<div class="bio-row">
														<p><span
																class="fa fa-location-arrow"> Village </span>:&nbsp;<?php echo
															$shareholders['Village']
															?></p>
													</div>
													<div class="bio-row">
														<p><span class="fa fa-send"> Email </span>:&nbsp;<?php echo
															$shareholders['Email']
															?></p>
													</div>
													<div class="bio-row">
														<p><span class="fa fa-envelope"> Box Number </span>:&nbsp;<?php echo
															$shareholders['Box']
															?></p>
													</div>
													<div class="bio-row">
														<p><span class="fa fa-phone-square"> Mobile </span>:&nbsp;<?php
															echo $shareholders['Telephone'] ?></p>
													</div>
												</div>
											</div>
											<!-- /.panel-body -->
										</div>
										<!-- /.panel -->
									</div>
								</div>
								<div id="shares" class="tab-pane fade">
									<hr/>
									<div class="col-md-12">
										<!-- Advanced Tables -->
										<?php if (($this->session->userdata('role') === "Admin")
										OR ($this->session->userdata('role') === "Finance")
										OR ($this->session->userdata('role') === "Admin2")) { ?>
										<div class="panel panel-pink">
											<?php } ?>
											<?php if (($this->session->userdata('role') === "Agent")) { ?>
											<div class="panel panel-green">
												<?php } ?>
												<div class="panel-heading">
													<div class="row">
														<div class="col-sm-6">
															<h3>Shares Subscribed</h3>
														</div>
													</div>
												</div>
												<div class="panel-body">
													<div class="table-responsive">
														<table class="table table-striped table-hover table-condensed"
														       id="stafftable">
															<thead>
															<tr>
																<?php foreach ($share as $field_name => $field_Column)
																	: ?>
																	<th><?php echo $field_Column ?></th>
																<?php endforeach; ?>
															</tr>
															</thead>
															<tbody>
															<?php foreach ($shares as $key => $data): ?>
																<tr>
																	<?php foreach ($share as $field_name => $field_Column): ?>
																		<td>
																			<?php echo $data->$field_name ?>
																		</td>
																	<?php endforeach; ?>
																</tr>
															<?php endforeach; ?>
															</tbody>
														</table>
													</div>
													<!-- /.row (nested) -->
												</div>
												<!-- /.panel-body -->
											</div>
											<!-- /.panel -->
										</div>
									</div>
									<div id="payments" class="tab-pane fade">
										<hr/>
										<div class="col-md-12">
											<!-- Advanced Tables -->
											<?php if (($this->session->userdata('role') === "Admin")
											OR ($this->session->userdata('role') === "Finance")
											OR ($this->session->userdata('role') === "Admin2")) { ?>
											<div class="panel panel-pink">
												<?php } ?>
												<?php if (($this->session->userdata('role') === "Agent")) { ?>
												<div class="panel panel-green">
													<?php } ?>
													<div class="panel-heading">
														<div class="row">
															<div class="col-sm-6">
																<h3>Shares Payments</h3>
															</div>
														</div>
													</div>
													<div class="panel-body">
														<div class="table-responsive">
															<table class="table table-striped table-hover table-condensed"
															       id="stafftable">
																<thead>
																<tr>
																	<?php foreach ($payment as $field_name => $field_Column)
																		: ?>
																		<th><?php echo $field_Column ?></th>
																	<?php endforeach; ?>
																</tr>
																</thead>
																<tbody>
																<?php foreach ($payments as $key => $data): ?>
																	<tr>
																		<?php foreach ($payment as $field_name => $field_Column): ?>
																			<td>
																				<?php echo $data->$field_name ?>
																			</td>
																		<?php endforeach; ?>
																	</tr>
																<?php endforeach; ?>
																</tbody>
															</table>
														</div>
														<!-- /.row (nested) -->
													</div>
													<!-- /.panel-body -->
												</div>
												<!-- /.panel -->
											</div>
										</div>
										<?php if ($type == 0) { ?>
										<div id="nok" class="tab-pane fade">
											<hr/>
											<div class="col-md-12">
												<!-- Advanced Tables -->
												<?php if (($this->session->userdata('role') === "Admin")
												OR ($this->session->userdata('role') === "Finance")
												OR ($this->session->userdata('role') === "Admin2")) { ?>
												<div class="panel panel-pink">
													<?php } ?>
													<?php if (($this->session->userdata('role') === "Agent")) { ?>
													<div class="panel panel-green">
														<?php } ?>
														<div class="panel-heading">
															<div class="row">
																<div class="col-sm-6">
																	<h3>Next of Kins</h3>
																</div>
															</div>
														</div>
														<div class="panel-body">
															<div class="table-responsive">
																<table
																	class="table table-striped table-hover table-condensed"
																	id="stafftable">
																	<thead>
																	<tr>
																		<?php foreach ($nok as $field_name => $field_Column): ?>
																			<th><?php echo $field_Column ?></th>
																		<?php endforeach; ?>
																	</tr>
																	</thead>
																	<tbody>
																	<?php foreach ($kin as $key => $data): ?>
																		<tr>
																			<?php foreach ($nok as $field_name => $field_Column): ?>
																				<td>
																					<?php echo $data->$field_name ?>
																				</td>
																			<?php endforeach; ?>
																		</tr>
																	<?php endforeach; ?>
																	</tbody>
																</table>
															</div>
															<!-- /.row (nested) -->
														</div>
														<!-- /.panel-body -->
													</div>
													<!-- /.panel -->
												</div>
											</div>
											<?php } ?>
											<!--End of all the tabs contents-->
										</div>
									</div>
								</div>
								<!--END CONTENT-->
