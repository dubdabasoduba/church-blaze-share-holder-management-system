<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Next of Kin Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-users"></i><a href='<?php echo base_url() . 'shareholders/shareholderAdd' ?>'>&nbsp;&nbsp;Shareholder
						Management</a></li>
				<li class="active"><i class="fa fa-user-plus"></i>&nbsp;&nbsp;Add Next of Kin</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					The panel helps in the searching and adding next of kins to the share holders!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Share Holder Search</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('shareholders/search2'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
											<label>Write the Share holder's Id Number to search</label>
										</div>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Enter Id Number", "name" => "search",
												"required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<?php echo form_submit('save', 'Search',
												'class="btn btn-success pull-left margin-right"'); ?>

										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<br/>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Add Next of Kins</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('shareholders/kinsave'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Names</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Next of Kin Names", "name" => "names",
												"required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Kin Identity</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Id Number / Birth Certificate", "name" => "identity",
												"required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Phone Number</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Phone number", "name" => "mobile")) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group pay">
											<label>Email Address</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Email Address", "name" => "email",
												"type" => "email")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<label>Relation</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Kin Relation", "name" => "relation",
											"required" => "true")) ?>
									</div>
									<div class="col-md-4">
										<label>Percentage</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Share Percentage", "name" => "percentage",
											"required" => "true")) ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Physical Form Number</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Physical Form Number", "name" => "fnumber",
												"required" => "true", "type" => "text")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Added By</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Location", "name" => "addedby", "readonly" => "true",
												"value" => $this->session->userdata('name'))) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<?php if ($view_data != null) { ?>
												<label>Share Holder Name</label>
												<?php echo form_input(array("class" => "form-control",
													"placeholder" => "Shareholder", "name" => "holder",
													"readonly" => "true", "value" => $view_data['Name'])) ?>
											<?php } ?>
										</div>
									</div>
									<div class="col-md-4 hidden">
										<?php if ($view_data != null) { ?>
											<label>Share Holder Id</label>
											<?php echo form_input(array("class" => "form-control", "name" => "Id",
												"value" => $view_data['Id'], "readonly" => "true")) ?>
										<?php } ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<?php if ($view_data != null) { ?>
												<?php echo form_hidden('kinidnumber', $view_data['IdNo'],
													'class="form-control"'); ?>
											<?php } ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<button type="reset" class="btn btn-danger pull-right">Cancel</button>
											<?php if ($view_data != null) { ?>
												<?php echo form_submit('add', 'Add Next Of Kin',
													'class="btn btn-success pull-right margin-right"'); ?>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
