<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Settings_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get($limit, $offset, $sort_by, $sort_order)
	{
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$sort_columns = array('type', 'Amount', 'sharetotal', 'sharesold', 'Description');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'type';

		$q = $this->db->select('*')->from('sharetypes')->limit($limit, $offset)->order_by($sort_by, $sort_order);

		$query = $q->get()->result();

		$q = $this->db->select('COUNT(*) as count', false)->from('sharetypes');
		$tmp = $q->get()->result();

		$result['rows'] = $query;
		$result['row_count'] = $tmp[0]->count;

		return $result;

	}

	public function getSysInfo()
	{
		$q = $this->db->select('*')->from('systeminfo');
		$query = $q->get()->result();
		$result['rows'] = $query;

		return $result;

	}

	public function getRegPay()
	{
		$q = $this->db->select('*')->from('regpaysetup');
		$query = $q->get()->result();
		$result['rows'] = $query;

		return $result;

	}

	public function editGet($id = null)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');

		if ($id != null) {
			$this->db->where(array('Id' => $id));
		}
		$query = $this->db->get();
		if ($id != null) {
			return $query->row_array();
		}
	}

	public function editGetReg($rpsId = null)
	{
		$this->db->select('*');
		$this->db->from('regpaysetup');

		if ($rpsId !== null) {
			$this->db->where(array('rpsId' => $rpsId));
		}
		$query = $this->db->get();
		if ($rpsId !== null) {
			return $query->row_array();
		}
	}

	public function remove($id)
	{
		$this->db->where('Id', $id);
		$this->db->delete('sharetypes');
	}

	public function removeSysInfo($id)
	{
		$this->db->where('Id', $id);
		$this->db->delete('systeminfo');
	}

	public function removeRegpay($rpsId)
	{
		$this->db->where('rpsId', $rpsId);
		$this->db->delete('regpaysetup');
	}

	public function addregpay($index)
	{
		if (isset($index['rpsId'])) {
			$this->db->where('rpsId', $index['rpsId']);
			$this->db->update('regpaysetup', $index);
		} else {
			$this->db->insert('regpaysetup', $index);
		}
	}

	public function add($index)
	{
		if (isset($index['Id'])) {
			$this->db->where('Id', $index['Id']);
			$this->db->update('sharetypes', $index);
		} else {
			$this->db->insert('sharetypes', $index);
		}
	}

	public function addSystemInfo($index)
	{
		if (isset($index['Id'])) {
			$this->db->where('Id', $index['Id']);
			$this->db->update('systeminfo', $index);
		} else {
			$this->db->insert('systeminfo', $index);
		}
	}

	public function editSystemInfo()
	{
		$this->db->select('*');
		$this->db->from('systeminfo');
		$query = $this->db->get();

		return $query->row_array();
	}

	public function editRegPay()
	{
		$this->db->select('*');
		$this->db->from('regpaysetup');
		$query = $this->db->get();

		return $query->row_array();
	}

	public function editSysInfo($id = null)
	{
		$this->db->select('*');
		$this->db->from('systeminfo');
		if ($id != null) {
			$this->db->where(array('Id' => $id));
		}
		$query = $this->db->get();
		if ($id != null) {
			return $query->row_array();
		}
	}

	public function editSysInfo1()
	{
		$this->db->select('*');
		$this->db->from('systeminfo');
		$query = $this->db->get();

		return $query->row_array();
	}

	public function getRegAmount()
	{
		$this->db->select('*');
		$this->db->from('regpaysetup');
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->amountPay;
		}
	}

	function getSystemInfo()
	{
		$q1 = $this->db->select('COUNT(*) as count', false)->from('systeminfo');
		$tmp = $q1->get()->result();
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}
}
