<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					System Settings
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-gears"></i>&nbsp;&nbsp;<a href="<?php echo base_url() . 'settings/settingsHome' ?>">System
						Settings</a></li>
				<li class="active"><i class="fa fa-plus"></i>&nbsp;&nbsp;Configure Registration Payment</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This form helps with the setting of the registration amount to allow registration payment in the system!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<div class="panel panel-pink">
					<div class="panel-heading">
						<div class="row">
							<div class="col-sm-6">
								<h3>Setup Registration Pay</h3>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<?php $this->load->helper('form'); ?>
						<?php echo form_open('settings/configurePay'); ?>
						<div class="form-body pal">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Name</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Name", "name" => "desc", "required" => "true")) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Amount</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Enter payment amount", "name" => "amount",
											"required" => "true")) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Added By</label>
										<?php echo form_input(array("class" => "form-control", "placeholder" => "Location",
											"name" => "addedby", "readonly" => "true",
											"value" => $this->session->userdata('name'))) ?>
									</div>
								</div>
							</div>
							<hr/>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
									</div>
								</div>
								<div class="col-md-8">
									<div class="form-group">
										<button type="reset" class="btn btn-danger pull-right">Cancel</button>
										<?php echo form_submit('save', 'Configure Registration Payment',
											'class="btn btn-success pull-right margin-right"'); ?>

									</div>
								</div>
							</div>
						</div>
						</form>
						<!-- /.row (nested) -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
