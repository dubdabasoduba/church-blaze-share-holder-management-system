<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					System Settings
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-gears"></i>&nbsp;&nbsp;<a href="<?php echo base_url() . 'settings/settingsHome' ?>">System
						Settings</a></li>
				<li class="active"><i class="fa fa-edit"></i>&nbsp;&nbsp;View Share Series</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This Displays the list of Share Series in Churchblaze!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($info) > 0) {
					?>
					<div class="alert alert-info" id="info"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $info . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<div class="panel panel-pink">
					<div class="panel-heading">
						<div class="row">
							<div class="col-sm-6">
								<h3>View Share Series</h3>
							</div>
							<div class="col-sm-6"><a class="btn btn-info btn-sm pull-right"
							                         href="<?php echo base_url() . "settings/addShareTypes" ?>">Add
									Share Series</a></div>
						</div>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-6">
							</div>
							<div class="col-sm-6">
								<p class=" records">Found&nbsp;<?php echo $rownumber; ?>&nbsp;Entries</p>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-hover table-condensed" id="stafftable">
								<thead>
								<tr>
									<?php foreach ($fields as $field_name => $field_Column): ?>
										<th <?php if ($sort_by == $field_name)
											echo " class=\"sort_$sort_order\"" ?>>
											<?php echo anchor("settings/viewShareTypes/$field_name/" . (($sort_order == "asc"
													&& $sort_by == $field_name) ? "desc" : "asc"), $field_Column) ?>
										</th>
									<?php endforeach; ?>
									<th><b class="text-red">Available Shares</b></th>
									<th><b class="text-red">Total Subscribed Amount</b></th>
									<th><b class="text-red">Unpaid Amount</b></th>
								</tr>
								</thead>
								<tbody>
								<?php foreach ($view_data as $key => $data): ?>
									<tr>
										<?php foreach ($fields as $field_name => $field_Column): ?>
											<td>
												<?php echo $data->$field_name ?>
											</td>
										<?php endforeach; ?>
										<td>
											<?php $total = $data->sharetotal; ?>
											<?php $used = $data->sharesold; ?>
											<b class="text-red"><?php echo($total - $used) ?></b>
										</td>
										<td>
											<?php $used = $data->sharesold; ?>
											<?php $price = $data->seriesPrice; ?>
											<?php $totalAmount = ($used * $price); ?>
											<b class="text-red"><?php echo($totalAmount) ?></b>
										</td>
										<td>
											<?php $used = $data->sharesold; ?>
											<?php $price = $data->seriesPrice; ?>
											<?php $totalAmount = ($used * $price); ?>
											<?php $paid = $data->amountreceived; ?>
											<b class="text-red"><?php echo($totalAmount - $paid) ?></b>
										</td>
										<td>
											<a class="btn btn-info btn-sm"
											   href="<?php echo base_url() . "settings/modify/" . $data->Id ?>">Edit</a>
										</td>
										<td>
											<a class="btn btn-danger btn-sm"
											   href="<?php echo base_url() . "settings/remove/" . $data->Id ?>"
											   onclick="return confirm('Are you sure you want to delete');">Delete</a>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
						<div class="row">
							<div class="col-sm-6">
							</div>
							<div class="col-sm-6">
								<?php if (strlen($pagination)) {
									;
								}
								{ ?>
									<p class=" records">Pages&nbsp;<?php echo $pagination; ?>&nbsp;</p>
								<?php } ?>
							</div>
						</div>
						<!-- /.row (nested) -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
