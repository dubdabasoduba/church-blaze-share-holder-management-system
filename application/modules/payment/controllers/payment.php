<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Payment extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('payment/payment_model', 'payment');
		$this->load->model('shareholders/shareholders_model', 'shareholders');
		$this->load->model('settings/settings_model', 'settings');
	}
	
	public function viewPayment()
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Admin2')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ('');
				$data['error'] = ('');
				$data['mainContent'] = 'viewPayment';
				$data['title'] = 'View Payment';
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
	
	public function viewRegPayment()
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Admin2')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ('');
				$data['error'] = ('');
				$data['mainContent'] = 'viewRegPay';
				$data['title'] = 'Registration Payment';
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
	
	public function receipt($Id = null)
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
			OR ($this->session->userdata('role') === 'Finance') OR ($this->session->userdata('role') === 'Admin2')
		) {
			if ($this->session->userdata('logged_in')) {
				if ($Id === null) {
					show_error('No shit found', 500);
				} else {
					$data['success'] = ('');
					$data['error'] = ('');
					$data['mainContent'] = 'paymentReceipt';
					$data['title'] = 'Print Receipt';
					$data['view_data1'] = $this->settings->editSysInfo1();
					$result = $this->payment->edit($Id);
					$data['view_data'] = $result;
					$type = $this->payment->getshareType($Id);
					$data['type'] = $type;
					$price = $this->payment->getsharePrice($Id);
					$data['price'] = $price;
					$data['balance'] = null;
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function regReceipt($payId = null)
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
			OR ($this->session->userdata('role') === 'Finance')
		) {
			if ($this->session->userdata('logged_in')) {
				if ($payId === null) {
					show_error('No shit found', 500);
				} else {
					$data['success'] = ('');
					$data['error'] = ('');
					$data['mainContent'] = 'regReceipt';
					$data['title'] = 'Registration Receipt';
					$data['view_data1'] = $this->settings->editSysInfo1();
					$result = $this->payment->editReg($payId);
					$data['view_data'] = $result;
					$amount = $this->payment->getReg2();
					$data['amount'] = $amount;
					$data['balance'] = null;
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
	
	public function viewPaymentSearch($sort_by = 'PaymentUUID', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Admin2')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ('');
				$data['success'] = ('');
				$data['error'] = ('');
				
				$limit = 15;
				$data['fields'] = array('physicalReceiptNumber' => 'CB Receipt #', 'PaymentUUID' => 'Unique Id',
					'Receiver' => 'Amount Receiver', 'PaymentType' => 'Payment Type', 'Amount' => 'Amount Paid',
					'numberofshares' => 'Share Paid For', 'date' => 'Payment Date', 'holderName' => 'Share Holder',
					'Code' => 'Payment Code');
				$receipt = $this->input->post('receipt');
				$result = $this->payment->get($receipt, $limit, $offset, $sort_by, $sort_order);
				
				$data['mainContent'] = 'viewPayment';
				$data['title'] = 'View Payment';
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url('index.php/payment/viewPayment/$sort_by/$sort_order/');
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();
				
				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				if ($result['rows'] == null) {
					$data['error'] = ('No share payment with the specified subscription receipt number was found');
				} else {
					$data['success'] = ('Share payment was retrieved successfully');
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
		
	}
	
	public function paymentHome()
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Admin2') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ('');
				$data['error'] = ('');
				$data['info'] = ('');
				$data['mainContent'] = 'paymentHome';
				$data['title'] = 'Payment Home';
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
	
	public function addRegPayemnt()
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
			OR ($this->session->userdata('role') === 'Finance') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ('');
				$data['error'] = ('');
				$data['info'] = ('');
				$data['mainContent'] = 'addRegPay';
				$data['title'] = 'Registration Payment';
				$result = $this->settings->getRegPay();
				$data['view_dataReg'] = $result['rows'];
				$data['view_data'] = null;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
		
	}
	
	public function holderSearch()
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
			OR ($this->session->userdata('role') === 'Agent') OR ($this->session->userdata('role') === 'Finance')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ('');
				$data['error'] = ('');
				$idnumber = $this->input->post('search');
				$result = $this->shareholders->get_search1($idnumber);
				$data['mainContent'] = 'addRegPay';
				$data['title'] = 'Registration Payment';
				$resultReg = $this->settings->getRegPay();
				$data['view_dataReg'] = $resultReg['rows'];
				$data['view_data'] = $result;
				if ($result == null) {
					$data['error'] =
						('No Share holder with the specified Id Number was found. Make sure that the user is Unapproved');
				} else {
					$data['success'] = ('Share holder was retrieved successfully');
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
	
	public function addPayment()
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Admin2') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ('');
				$data['error'] = ('');
				$data['info'] = ('');
				$receipt = null;
				$result = $this->payment->get_receipt($receipt);
				$sharetypes = $this->payment->get_shareType($receipt);
				$shareprice = $this->payment->getShareAmount($receipt);
				$amountreceived = $this->payment->getReceivedAmount($receipt);
				$holders = $this->payment->get_shareHolders($receipt);
				$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
				$data['mainContent'] = 'addPayment';
				$data['title'] = 'Add Payment';
				$data['view_data'] = $result;
				$data['shareprice'] = $shareprice;
				$data['amountreceived'] = $amountreceived;
				$data['sharetypes'] = $sharetypes;
				$data['holders'] = $holders;
				$data['idnumber'] = $idnumber;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
		
	}
	
	public function receiptSearch()
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Admin2') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ('');
				$data['error'] = ('');
				$data['info'] = ('');
				$receipt = $this->input->post('receipt');
				$result = $this->payment->get_receipt($receipt);
				$sharetypes = $this->payment->get_shareType($receipt);
				$shareprice = $this->payment->getShareAmount($receipt);
				$amountreceived = $this->payment->getReceivedAmount($receipt);
				$holders = $this->payment->get_shareHolders($receipt);
				$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
				$data['idnumber'] = $idnumber;
				$data['holders'] = $holders;
				$data['amountreceived'] = $amountreceived;
				$data['sharetypes'] = $sharetypes;
				$data['shareprice'] = $shareprice;
				$data['mainContent'] = 'addPayment';
				$data['title'] = 'Add Payment';
				$data['view_data'] = $result;
				if ($result == null) {
					$data['error'] = ('No share subscription with the specified receipt number was found');
				} else {
					$data['success'] = ('Share subscription was retrieved successfully');
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
	
	public function modify($id = null)
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Admin2')
		) {
			if ($this->session->userdata('logged_in')) {
				if ($id === null) {
					show_error('No shit found', 500);
				} else {
					$receipt = $this->payment->getreceipt($id);
					$sharetypes = $this->payment->get_shareType($receipt);
					$holders = $this->payment->get_shareHolders($receipt);
					$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
					$sharesbought = $this->payment->getSharesBought($receipt);
					$shareprice = $this->payment->getShareAmount2($id);
					$amountreceived = $this->payment->getReceivedAmount2($id);
					$index['view_data'] = $this->payment->edit($id);
					$index['idnumber'] = $idnumber;
					$index['holders'] = $holders;
					$index['shareprice'] = $shareprice;
					$index['amountreceived'] = $amountreceived;
					$index['sharetypes'] = $sharetypes;
					$index['sharesbought'] = $sharesbought;
					$index['success'] = ('');
					$index['error'] = ('');
					$index['info'] = ('');
					$index['mainContent'] = 'editPayment';
					$index['title'] = 'Edit Payment';
					
					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function modifyReg($payId = null)
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Admin2')
		) {
			if ($this->session->userdata('logged_in')) {
				if ($payId === null) {
					show_error('No shit found', 500);
				} else {

					$result = $this->payment->editReg($payId);
					$index['view_data'] = $result;
					$index['success'] = ('');
					$index['error'] = ('');
					$index['info'] = ('');
					$index['mainContent'] = 'editRegPay';
					$index['title'] = 'Registration Payment';
					$resultReg = $this->settings->getRegPay();
					$index['view_dataReg'] = $resultReg['rows'];
					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
	
	public function remove($id = null)
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Admin2')
		) {
			if ($this->session->userdata('logged_in')) {
				if ($id === null) {
					show_error('No shit provided', 500);
				} else {
					$typeID = $this->payment->getshareTypeId($id);
					$paid = $this->payment->getPaymentTotal($id);
					$amountreceived = $this->payment->getReceivedAmount2($id);
					$newtotal = ($amountreceived - $paid);
					$user['Id'] = $typeID;
					$user['amountreceived'] = $newtotal;
					$this->payment->updateShareType($user);
					$this->payment->remove($id);
					$data['success'] = ('');
					$data['error'] = ('');
					$data['info'] = ('');
					$data['mainContent'] = 'viewPayment';
					$data['title'] = 'View Payment';
					$data['view_data'] = null;
					$this->load->view('Admin/adminRedirect', $data);
					$data['success'] = ('Payment deleted successfully');
					
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
		
	}

	public function removeReg($payId = null)
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Admin2')
		) {
			if ($this->session->userdata('logged_in')) {
				if ($payId == null) {
					show_error('No shit provided', 500);
				} else {
					$this->payment->removeReg($payId);
					$data['success'] = ('');
					$data['error'] = ('');
					$data['info'] = ('');
					$data['mainContent'] = 'viewRegPay';
					$data['title'] = 'Registration Payment';
					$data['view_data'] = null;
					$data['success'] = ('Payment deleted successfully');
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}
	
	public function save()
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Admin2') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ('');
				$data['error'] = ('');
				$data['info'] = ('');
				$this->load->library('form_validation');
				$this->form_validation->set_rules('shares', 'Number of shares', 'required|trim');
				$this->form_validation->set_rules('paid', 'Amount Paid', 'required|trim');
				$this->form_validation->set_rules('fnumber', 'Physical Receipt Number', 'required|trim');
				$type = $this->input->post('type');
				if ($type == 'M-Pesa' OR $type == 'Cheque' OR $type == 'Bank Deposit') {
					$this->form_validation->set_rules('code', 'Payment Code', 'required|trim');
				}
				$idnumber = $this->input->post('idnumber');
				$shares = $this->input->post('shares');
				$paid = $this->input->post('paid');
				$code = $this->input->post('code');
				$holdername = $this->input->post('holdername');
				$bought = $this->input->post('bought');
				$buyId = $this->input->post('buyId');
				$sharetype = $this->input->post('sharetype');
				$shareprice = $this->input->post('price');
				$shareholder = $this->input->post('shareholder');
				$Receiptnumber = $this->input->post('Receiptnumber');
				$receiver = $this->input->post('receiver');
				$amountreceived = $this->input->post('amountreceived');
				$sharepayment = ($shares * $shareprice);
				$fnumber = $this->input->post('fnumber');
				if ($type != 'choose') {
					if (($this->form_validation->run()) AND ($paid != 0) AND ($paid != 0)) {
						$receipt = $this->payment->receiptNumber();
						
						if (($bought > $shares) OR ($bought == $shares)) {
							if (($sharepayment < $paid) OR ($sharepayment == $paid)) {
								if (isset($_POST) && $_POST['save'] === 'Add Payment') {
									$index['PaymentUUID'] = $receipt;
									$index['shareType'] = $sharetype;
									$index['Amount'] = $sharepayment;
									$index['numberofshares'] = $shares;
									$index['holder'] = $shareholder;
									$index['holderName'] = $holdername;
									$index['PaymentType'] = $type;
									$index['buyId'] = $buyId;
									$index['physicalReceiptNumber'] = $fnumber;
									$index['buyreceipt'] = $Receiptnumber;
									$index['Receiver'] = $receiver;
									$index['AddedBy'] = $receiver;
									$index['dateadded'] = date('Y-m-d');
									$index['idnumber'] = $idnumber;
									if ($type === 'Cash') {
										$index['Code'] = 'N/A on Cash Payment';
									} else {
										$index['Code'] = $code;
									}
									$this->payment->addreg($index);
									$user['Id'] = $sharetype;
									$user['amountreceived'] = ($amountreceived + $sharepayment);
									$this->payment->updateShareType($user);
									$data['success'] = ('');
									$data['error'] = ('');
									$data['info'] = ('');
									$data['mainContent'] = 'paymentReceipt';
									$data['title'] = 'Print Receipt';
									$data['view_data1'] = $this->settings->editSysInfo1();
									$result = $this->payment->edit2($receipt);
									$data['view_data'] = $result;
									$type = $this->payment->getshareType2($receipt);
									$data['type'] = $type;
									$price = $this->payment->getsharePrice2($receipt);
									$data['price'] = $price;
									$data['balance'] = ($paid - $sharepayment);
									$data['amount'] = $paid;
									$data['total'] = $sharepayment;
									$this->load->view('Admin/adminRedirect', $data);
								} else {
									$data['success'] = ('');
									$data['error'] = ('');
									$data['info'] = ('');
									$receipt = null;
									$result = $this->payment->get_receipt($receipt);
									$sharetypes = $this->payment->get_shareType($receipt);
									$holders = $this->payment->get_shareHolders($receipt);
									$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
									$shareprice = $this->payment->getShareAmount($receipt);
									$amountreceived = $this->payment->getReceivedAmount($receipt);
									$data['idnumber'] = $idnumber;
									$data['mainContent'] = 'addPayment';
									$data['title'] = 'Add Payment';
									$data['view_data'] = $result;
									$data['sharetypes'] = $sharetypes;
									$data['shareprice'] = $shareprice;
									$data['amountreceived'] = $amountreceived;
									$data['holders'] = $holders;
									$data['error'] = ('Payment could not be added successfully. Contact System Admin');

									$this->load->view('Admin/adminRedirect', $data);
								}
							} else {
								$data['success'] = ('');
								$data['error'] = ('');
								$data['info'] = ('');
								$receipt = $Receiptnumber;
								$result = $this->payment->get_receipt($receipt);
								$sharetypes = $this->payment->get_shareType($receipt);
								$holders = $this->payment->get_shareHolders($receipt);
								$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
								$shareprice = $this->payment->getShareAmount($receipt);
								$amountreceived = $this->payment->getReceivedAmount($receipt);
								$data['idnumber'] = $idnumber;
								$data['mainContent'] = 'addPayment';
								$data['title'] = 'Add Payment';
								$data['view_data'] = $result;
								$data['sharetypes'] = $sharetypes;
								$data['shareprice'] = $shareprice;
								$data['amountreceived'] = $amountreceived;
								$data['holders'] = $holders;
								$data['error'] =
									('The amount entered is less than the amount required to pay for = ' . $shares
										. ' please reduce the number of share to pay for');
								
								$this->load->view('Admin/adminRedirect', $data);
							}
						} else {
							$data['success'] = ('');
							$data['error'] = ('');
							$data['info'] = ('');
							$receipt = $Receiptnumber;
							$result = $this->payment->get_receipt($receipt);
							$sharetypes = $this->payment->get_shareType($receipt);
							$holders = $this->payment->get_shareHolders($receipt);
							$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
							$shareprice = $this->payment->getShareAmount($receipt);
							$amountreceived = $this->payment->getReceivedAmount($receipt);
							$data['idnumber'] = $idnumber;
							$data['mainContent'] = 'addPayment';
							$data['title'] = 'Add Payment';
							$data['view_data'] = $result;
							$data['sharetypes'] = $sharetypes;
							$data['shareprice'] = $shareprice;
							$data['amountreceived'] = $amountreceived;
							$data['holders'] = $holders;
							$data['error'] =
								('You can not pay for shares more than what you bought. The number of shares bought are = '
									. $bought . ' Shares');
							
							$this->load->view('Admin/adminRedirect', $data);
						}
						
					} else {
						$data['success'] = ('');
						$data['error'] = ('');
						$data['info'] = ('');
						$receipt = $Receiptnumber;
						$result = $this->payment->get_receipt($receipt);
						$sharetypes = $this->payment->get_shareType($receipt);
						$holders = $this->payment->get_shareHolders($receipt);
						$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
						$shareprice = $this->payment->getShareAmount($receipt);
						$amountreceived = $this->payment->getReceivedAmount($receipt);
						$data['idnumber'] = $idnumber;
						$data['mainContent'] = 'addPayment';
						$data['title'] = 'Add Payment';
						$data['view_data'] = $result;
						$data['sharetypes'] = $sharetypes;
						$data['shareprice'] = $shareprice;
						$data['amountreceived'] = $amountreceived;
						$data['holders'] = $holders;
						$data['error'] = ('Please fill all the spaces appropriately');
						
						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ('');
					$data['error'] = ('');
					$data['info'] = ('');
					$receipt = $Receiptnumber;
					$result = $this->payment->get_receipt($receipt);
					$sharetypes = $this->payment->get_shareType($receipt);
					$holders = $this->payment->get_shareHolders($receipt);
					$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
					$shareprice = $this->payment->getShareAmount($receipt);
					$amountreceived = $this->payment->getReceivedAmount($receipt);
					$data['idnumber'] = $idnumber;
					$data['mainContent'] = 'addPayment';
					$data['title'] = 'Add Payment';
					$data['view_data'] = $result;
					$data['sharetypes'] = $sharetypes;
					$data['shareprice'] = $shareprice;
					$data['amountreceived'] = $amountreceived;
					$data['holders'] = $holders;
					$data['error'] = ('Please Choose a payment type');
					
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
		
	}
	
	public function update($id = null)
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Admin2') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ('');
				$data['error'] = ('');
				$data['info'] = ('');
				$this->load->library('form_validation');
				$type = $this->input->post('type');
				if ($type == 'M-Pesa' OR $type == 'Cheque' OR $type == 'Bank Deposit') {
					$this->form_validation->set_rules('code', 'Payment Code', 'required|trim');
				}
				$this->form_validation->set_rules('shares', 'Number of shares', 'required|trim');
				$this->form_validation->set_rules('paid', 'Amount Paid', 'required|trim');
				$this->form_validation->set_rules('fnumber', 'Physical Receipt Number', 'required|trim');
				$idnumber = $this->input->post('idnumber');
				$shares = $this->input->post('shares');
				$paid = $this->input->post('paid');
				$code = $this->input->post('code');
				$holdername = $this->input->post('holdername');
				$bought = $this->input->post('bought');
				$buyId = $this->input->post('buyId');
				$sharetype = $this->input->post('sharetype');
				$shareprice = $this->input->post('price');
				$receipt = $this->input->post('paycode');
				$Receiptnumber = $this->input->post('Receiptnumber');
				$receiver = $this->input->post('receiver');
				$amountreceived = $this->input->post('amountreceived');
				$oldnoshare = $this->input->post('oldnoshare');
				$oldpaid = $this->input->post('oldpaid');
				$oldtotal = ($oldnoshare * $oldpaid);
				$sharepayment = ($shares * $shareprice);
				$fnumber = $this->input->post('fnumber');
				if ($type != 'choose') {
					if (($this->form_validation->run()) AND ($paid != 0) AND ($paid != 0)) {
						if (($bought > $shares) OR ($bought == $shares)) {
							if (($sharepayment < $paid) OR ($sharepayment == $paid)) {
								if (isset($_POST) && $_POST['update'] === 'Update Payment') {
									$index['Id'] = $this->input->post('Id');
									$index['Amount'] = $sharepayment;
									$index['numberofshares'] = $shares;
									$index['PaymentType'] = $type;
									$index['Receiver'] = $receiver;
									$index['physicalReceiptNumber'] = $fnumber;
									$index['UpdatedBy'] = $this->input->post('Updatedby');
									$index['dateupdated'] = date('Y-m-d');
									if ($type === 'Cash') {
										$index['Code'] = 'N/A on Cash Payment';
									} else {
										$index['Code'] = $code;
									}
									$this->payment->addreg($index);
									$user['Id'] = $sharetype;
									$user['amountreceived'] = (($amountreceived + $sharepayment) - $oldpaid);
									$this->payment->updateShareType($user);
									$data['success'] = ('');
									$data['error'] = ('');
									$data['info'] = ('');
									$data['mainContent'] = 'paymentReceipt';
									$data['title'] = 'Print Receipt';
									$data['view_data1'] = $this->settings->editSysInfo1();
									$result = $this->payment->edit2($receipt);
									$data['view_data'] = $result;
									$type = $this->payment->getshareType2($receipt);
									$data['type'] = $type;
									$price = $this->payment->getsharePrice2($receipt);
									$data['price'] = $price;
									$data['balance'] = ($paid - $sharepayment);
									$data['amount'] = $paid;
									$data['total'] = $sharepayment;
									$this->load->view('Admin/adminRedirect', $data);
								} else {
									$data['success'] = ('');
									$data['error'] = ('');
									$data['info'] = ('');
									$receipt = $this->payment->getreceipt($this->input->post('Id'));
									$sharetypes = $this->payment->get_shareType($receipt);
									$holders = $this->payment->get_shareHolders($receipt);
									$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
									$sharesbought = $this->payment->getSharesBought($receipt);
									$shareprice = $this->payment->getShareAmount2($this->input->post('Id'));
									$amountreceived = $this->payment->getReceivedAmount2($this->input->post('Id'));
									$data['mainContent'] = 'editPayment';
									$data['title'] = 'Edit Payment';
									$data['view_data'] = $this->payment->edit($this->input->post('Id'));
									$data['idnumber'] = $idnumber;
									$data['holders'] = $holders;
									$data['shareprice'] = $shareprice;
									$data['amountreceived'] = $amountreceived;
									$data['sharetypes'] = $sharetypes;
									$data['sharesbought'] = $sharesbought;
									$data['error'] = ('Payment could not be added successfully. Contact System Admin');

									$this->load->view('Admin/adminRedirect', $data);
								}
							} else {
								$data['success'] = ('');
								$data['error'] = ('');
								$data['info'] = ('');
								$receipt = $this->payment->getreceipt($this->input->post('Id'));
								$sharetypes = $this->payment->get_shareType($receipt);
								$holders = $this->payment->get_shareHolders($receipt);
								$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
								$sharesbought = $this->payment->getSharesBought($receipt);
								$shareprice = $this->payment->getShareAmount2($this->input->post('Id'));
								$amountreceived = $this->payment->getReceivedAmount2($this->input->post('Id'));
								$data['mainContent'] = 'editPayment';
								$data['title'] = 'Edit Payment';
								$data['view_data'] = $this->payment->edit($this->input->post('Id'));
								$data['idnumber'] = $idnumber;
								$data['holders'] = $holders;
								$data['shareprice'] = $shareprice;
								$data['amountreceived'] = $amountreceived;
								$data['sharetypes'] = $sharetypes;
								$data['sharesbought'] = $sharesbought;
								$data['error'] = ('The amount entered is than the amount required to pay for = ' . $shares
									. ' please reduce the number of share to pay for');
								
								$this->load->view('Admin/adminRedirect', $data);
							}
						} else {
							$data['success'] = ('');
							$data['error'] = ('');
							$data['info'] = ('');
							$receipt = $this->payment->getreceipt($this->input->post('Id'));
							$sharetypes = $this->payment->get_shareType($receipt);
							$holders = $this->payment->get_shareHolders($receipt);
							$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
							$sharesbought = $this->payment->getSharesBought($receipt);
							$shareprice = $this->payment->getShareAmount2($this->input->post('Id'));
							$amountreceived = $this->payment->getReceivedAmount2($this->input->post('Id'));
							$data['mainContent'] = 'editPayment';
							$data['title'] = 'Edit Payment';
							$data['view_data'] = $this->payment->edit($this->input->post('Id'));
							$data['idnumber'] = $idnumber;
							$data['holders'] = $holders;
							$data['shareprice'] = $shareprice;
							$data['amountreceived'] = $amountreceived;
							$data['sharetypes'] = $sharetypes;
							$data['sharesbought'] = $sharesbought;
							$data['error'] =
								('You can not pay for shares more than what you bought. The number of shares bought are = '
									. $bought . ' Shares');
							
							$this->load->view('Admin/adminRedirect', $data);
						}
						
					} else {
						$data['success'] = ('');
						$data['error'] = ('');
						$data['info'] = ('');
						$receipt = $this->payment->getreceipt($this->input->post('Id'));
						$sharetypes = $this->payment->get_shareType($receipt);
						$holders = $this->payment->get_shareHolders($receipt);
						$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
						$sharesbought = $this->payment->getSharesBought($receipt);
						$shareprice = $this->payment->getShareAmount2($this->input->post('Id'));
						$amountreceived = $this->payment->getReceivedAmount2($this->input->post('Id'));
						$data['mainContent'] = 'editPayment';
						$data['title'] = 'Edit Payment';
						$data['view_data'] = $this->payment->edit($this->input->post('Id'));
						$data['idnumber'] = $idnumber;
						$data['holders'] = $holders;
						$data['shareprice'] = $shareprice;
						$data['amountreceived'] = $amountreceived;
						$data['sharetypes'] = $sharetypes;
						$data['sharesbought'] = $sharesbought;
						$data['error'] = ('Please fill all the spaces appropriately');
						
						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ('');
					$data['error'] = ('');
					$data['info'] = ('');
					$receipt = $this->payment->getreceipt($this->input->post('Id'));
					$sharetypes = $this->payment->get_shareType($receipt);
					$holders = $this->payment->get_shareHolders($receipt);
					$idnumber = $this->payment->get_shareHoldersIdnumber($receipt);
					$sharesbought = $this->payment->getSharesBought($receipt);
					$shareprice = $this->payment->getShareAmount2($this->input->post('Id'));
					$amountreceived = $this->payment->getReceivedAmount2($this->input->post('Id'));
					$data['mainContent'] = 'editPayment';
					$data['title'] = 'Edit Payment';
					$data['view_data'] = $this->payment->edit($this->input->post('Id'));
					$data['idnumber'] = $idnumber;
					$data['holders'] = $holders;
					$data['shareprice'] = $shareprice;
					$data['amountreceived'] = $amountreceived;
					$data['sharetypes'] = $sharetypes;
					$data['sharesbought'] = $sharesbought;
					$data['error'] = ('Please Choose a payment type');
					
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
		
	}
	
	public function search($sort_by = 'Id', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')
			OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ('');
				$data['error'] = ('');
				$limit = 5;
				$data['fields'] = array('Id' => 'Id', 'Name' => 'Names', 'IdNo' => 'Id Number', 'Telephone' => 'Telephone',
					'Email' => 'Email');
				
				$index = $this->input->post('search');
				$result = $this->shareholders->get_search($index, $limit, $offset, $sort_by, $sort_order);
				$data['mainContent'] = 'addPayment';
				$data['title'] = 'Add Payment';
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];
				
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url('index.php/payment/addPayment/$sort_by/$sort_order/');
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();
				
				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
	
	public function saveRegPayment()
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
			OR ($this->session->userdata('role') === 'Finance') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ('');
				$data['error'] = ('');
				$data['info'] = ('');
				$this->load->library('form_validation');
				$type = $this->input->post('type');

				if ($type == 'M-Pesa' OR $type == 'Cheque' OR $type == 'Bank Deposit') {
					$this->form_validation->set_rules('code', 'Payment Code', 'required|trim');
				}
				$this->form_validation->set_rules('amount', 'Amount Paid', 'required|trim');
				$this->form_validation->set_rules('cbamount', 'Choose Registration Amount by churchblaze', 'required|trim');
				$this->form_validation->set_rules('fnumber', 'Physical Receipt Number', 'required|trim');
				$idnumber = $this->input->post('idnumber');
				$kra = $this->input->post('kra');
				$amount = $this->input->post('amount');
				$typeuser = $this->input->post('typeuser');
				$code = $this->input->post('code');
				$shareholder = $this->input->post('holder');
				$shareholderId = $this->input->post('Id');
				$fnumber = $this->input->post('fnumber');
				$pay = $this->input->post('cbamount');
				$paid = $this->payment->getReg1($shareholderId);
				if (($this->form_validation->run()) AND ($amount != 0)) {
					if ($paid != $shareholderId) {
						if ($type != 'choose') {
							if ($pay != 0) {
								$receipt = $this->payment->regReceipt();
								$change = ($pay - $amount);
								if ($amount > $pay OR $amount == $pay) {
									if (isset($_POST) && $_POST['add'] === 'Add Registration Payment') {
										$index['shareholder'] = $shareholder;
										if ($typeuser == 0) {
											$index['idnumber'] = $idnumber;
										} else {
											$index['idnumber'] = $kra;
										}
										if ($type == 'Cash') {
											$index['code'] = 'N/A for Cash Payment';
										} else {
											$index['code'] = $code;
										}
										$index['paymentmode'] = $type;
										$index['shareholderId'] = $shareholderId;
										$index['amount'] = $pay;
										$index['receipt'] = $receipt;
										$index['physicalRegReceiptNumber'] = $fnumber;
										$index['receiver'] = $this->session->userdata('name');
										$index['AddedBy'] = $this->input->post('addedby');
										$index['dateadded'] = date('Y-m-d');
										$this->payment->add($index);
										$data['mainContent'] = 'regReceipt';
										$data['title'] = 'Registration Receipt';
										$data['view_data1'] = $this->settings->editSysInfo1();
										$result = $this->payment->editReg1($receipt);
										$data['view_data'] = $result;
										$pay1 = $this->payment->getReg2();
										$data['pay1'] = $pay1;
										$data['amount'] = $amount;
										$data['balance'] = $amount - $pay;
										$this->load->view('Admin/adminRedirect', $data);
									} else {
										$data['success'] = ('');
										$data['error'] = ('');
										$data['info'] = ('');
										$result = $this->shareholders->get_search1($idnumber);;
										$data['mainContent'] = 'addRegPay';
										$data['title'] = 'Registration Payment';
										$data['view_data'] = $result;
										$resultReg = $this->settings->getRegPay();
										$data['view_dataReg'] = $resultReg['rows'];
										$data['error'] = ('Payment could not be added. Contact System Admin');

										$this->load->view('Admin/adminRedirect', $data);
									}
								} else {
									$data['success'] = ('');
									$data['error'] = ('');
									$data['info'] = ('');
									$result = $this->shareholders->get_search1($idnumber);
									$data['mainContent'] = 'addRegPay';
									$data['title'] = 'Registration Payment';
									$data['view_data'] = $result;
									$resultReg = $this->settings->getRegPay();
									$data['view_dataReg'] = $resultReg['rows'];
									$data['error'] =
										('The amount entered is less than the required amount. Enter any amount above '
											. $pay);

									$this->load->view('Admin/adminRedirect', $data);
								}
							} else {
								$data['success'] = ('');
								$data['error'] = ('');
								$data['info'] = ('');
								$result = $this->shareholders->get_search1($idnumber);
								$data['mainContent'] = 'addRegPay';
								$data['title'] = 'Registration Payment';
								$data['view_data'] = $result;
								$resultReg = $this->settings->getRegPay();
								$data['view_dataReg'] = $resultReg['rows'];
								$data['error'] = ('Please make sure you select a registration amount from the list');

								$this->load->view('Admin/adminRedirect', $data);
							}
						} else {
							$data['success'] = ('');
							$data['error'] = ('');
							$data['info'] = ('');
							$result = $this->shareholders->get_search1($idnumber);
							$data['mainContent'] = 'addRegPay';
							$data['title'] = 'Registration Payment';
							$data['view_data'] = $result;
							$resultReg = $this->settings->getRegPay();
							$data['view_dataReg'] = $resultReg['rows'];
							$data['error'] = ('Please make sure you select a payment mode to process payment');

							$this->load->view('Admin/adminRedirect', $data);
						}
					} else {
						$data['success'] = ('');
						$data['error'] = ('');
						$data['info'] = ('');
						$result = $this->shareholders->get_search1($idnumber);
						$data['mainContent'] = 'addRegPay';
						$data['title'] = 'Registration Payment';
						$data['view_data'] = $result;
						$resultReg = $this->settings->getRegPay();
						$data['view_dataReg'] = $resultReg['rows'];
						$data['error'] = ('A share holder cannot pay for registration twice!!');

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {

					$data['success'] = ('');
					$data['error'] = ('');
					$data['info'] = ('');
					$result = $this->shareholders->get_search1($idnumber);
					$data['mainContent'] = 'addRegPay';
					$data['title'] = 'Registration Payment';
					$data['view_data'] = $result;
					$resultReg = $this->settings->getRegPay();
					$data['view_dataReg'] = $resultReg['rows'];
					$data['error'] = ('Please fill all the spaces appropriately');

					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function updateRegPayment()
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
			OR ($this->session->userdata('role') === 'Finance') OR ($this->session->userdata('role') === 'Agent')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ('');
				$data['error'] = ('');
				$data['info'] = ('');
				$this->load->library('form_validation');
				$type = $this->input->post('type');

				if ($type == 'M-Pesa' OR $type == 'Cheque' OR $type == 'Bank Deposit') {
					$this->form_validation->set_rules('code', 'Payment Code', 'required|trim');
				}
				$this->form_validation->set_rules('amount', 'Number of shares', 'required|trim');
				$this->form_validation->set_rules('cbamount', 'Choose Registration Amount by churchblaze', 'required|trim');
				$this->form_validation->set_rules('fnumber', 'Physical Receipt Number', 'required|trim');
				$idnumber = $this->input->post('idnumber');
				$updatedby = $this->input->post('updatedby');
				$amount = $this->input->post('amount');
				$payId = $this->input->post('payId');
				$code = $this->input->post('code');
				$receipt = $this->input->post('receipt');
				$dateadded = $this->input->post('dateadded');
				$pay = $this->input->post('cbamount');
				$fnumber = $this->input->post('fnumber');
				if ($type != 'choose') {
					if ($pay != 0) {
						if (($this->form_validation->run()) AND ($amount != 0)) {
							$change = ($pay - $amount);
							if ($amount > $pay OR $amount == $pay) {
								if (isset($_POST) && $_POST['update'] === 'Update Registration Payment') {
									if ($type == 'Cash') {
										$index['code'] = 'N/A for Cash Payment';
									} else {
										$index['code'] = $code;
									}
									$index['paymentmode'] = $type;
									$index['dateadded'] = $dateadded;
									$index['amount'] = $pay;
									$index['payId'] = $payId;
									$index['physicalRegReceiptNumber'] = $fnumber;
									$index['receiver'] = $this->session->userdata('name');
									$index['AddedBy'] = $this->input->post('addedby');
									$index['AddedBy'] = $updatedby;
									$index['dateupdated'] = date('Y-m-d');
									$this->payment->add($index);
									$data['mainContent'] = 'regReceipt';
									$data['title'] = 'Registration Receipt';
									$data['view_data1'] = $this->settings->editSysInfo1();
									$result = $this->payment->editReg1($receipt);
									$data['view_data'] = $result;
									$pay1 = $this->payment->getReg2();
									$data['pay1'] = $pay1;
									$data['amount'] = $amount;
									$data['balance'] = $amount - $pay;
									$this->load->view('Admin/adminRedirect', $data);
								} else {
									$data['success'] = ('');
									$data['error'] = ('');
									$data['mainContent'] = 'editRegPay';
									$data['title'] = 'Registration Payment';
									$result = $this->payment->editReg($payId);
									$data['view_data'] = $result;
									$resultReg = $this->settings->getRegPay();
									$data['view_dataReg'] = $resultReg['rows'];
									$data['error'] = ('Payment could not be added. Contact System Admin');

									$this->load->view('Admin/adminRedirect', $data);
								}
							} else {
								$data['success'] = ('');
								$data['error'] = ('');
								$data['info'] = ('');
								$data['mainContent'] = 'editRegPay';
								$data['title'] = 'Registration Payment';
								$result = $this->payment->editReg($payId);
								$data['view_data'] = $result;
								$resultReg = $this->settings->getRegPay();
								$data['view_dataReg'] = $resultReg['rows'];
								$data['error'] =
									('The amount entered is less than the required amount. Enter any amount above '
										. $pay);

								$this->load->view('Admin/adminRedirect', $data);
							}
						} else {

							$data['success'] = ('');
							$data['error'] = ('');
							$data['info'] = ('');
							$data['mainContent'] = 'editRegPay';
							$data['title'] = 'Registration Payment';
							$result = $this->payment->editReg($payId);
							$data['view_data'] = $result;
							$resultReg = $this->settings->getRegPay();
							$data['view_dataReg'] = $resultReg['rows'];
							$data['error'] = ('Please fill all the spaces appropriately');

							$this->load->view('Admin/adminRedirect', $data);
						}
					} else {
						$data['success'] = ('');
						$data['error'] = ('');
						$data['info'] = ('');
						$data['mainContent'] = 'editRegPay';
						$data['title'] = 'Registration Payment';
						$result = $this->payment->editReg($payId);
						$data['view_data'] = $result;
						$resultReg = $this->settings->getRegPay();
						$data['view_dataReg'] = $resultReg['rows'];
						$data['error'] = ('Please make sure you select a registration amount from the list');
						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ('');
					$data['error'] = ('');
					$data['info'] = ('');
					$data['mainContent'] = 'editRegPay';
					$data['title'] = 'Registration Payment';
					$result = $this->payment->editReg($payId);
					$data['view_data'] = $result;
					$resultReg = $this->settings->getRegPay();
					$data['view_dataReg'] = $resultReg['rows'];
					$data['error'] = ('Please make sure you select a payment mode to process payment');

					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function viewSearch($sort_by = 'payId', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Admin2')
			OR ($this->session->userdata('role') === 'Finance')
		) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ('');
				$data['success'] = ('');
				$data['error'] = ('');
				$limit = 5;
				$data['fields'] = array('physicalRegReceiptNumber' => 'CB Receipt #', 'receipt' => 'Receipt Number',
					'paymentmode' => 'Payment Mode', 'code' => 'Payment Code', 'amount' => 'Amount Paid',
					'datepaid' => 'Date Paid', 'shareholder' => 'Share Holder', 'receiver' => 'Receiver');
				$idnumber = $this->input->post('search');
				$result = $this->payment->getReg($idnumber, $limit, $offset, $sort_by, $sort_order);
				$data['mainContent'] = 'viewRegPay';
				$data['title'] = 'Registration Payment';
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url('index.php/payment/viewSearch/$sort_by/$sort_order/');
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				if ($result['rows'] == null) {
					$data['error'] = ('No Registration payment was found for the user Id Number entered');
				} else {
					$data['success'] = ('Registration payment was retrieved successfully');
				}
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}
}
