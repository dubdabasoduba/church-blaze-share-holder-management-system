<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Payment Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-money"></i><a href='<?php echo base_url() . 'payment/paymentHome' ?>'>&nbsp;&nbsp;Payment
						Management</a></li>
				<li class="active"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Registration Payment</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This helps in the addition of the registration payment!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Finance")
				OR ($this->session->userdata('role') === "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') === "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Share Holder Search</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('payment/holderSearch'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
											<label>Write the Share holder's Id Number / KRA pin to search</label>
										</div>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Enter Id Number / KRA Pin Number", "name" => "search",
												"required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<?php echo form_submit('save', 'Search',
												'class="btn btn-success pull-left margin-right"'); ?>

										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<br/>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Finance")
				OR ($this->session->userdata('role') === "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') === "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Registration Payment</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('payment/saveRegPayment'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Choose Registration Amount by churchblaze</label>
											<select class="form-control" id="cbamount" name="cbamount">
												<option value="0">Please Select One....</option>
												<?php foreach ($view_dataReg as $data): ?>
													<option
														value="<?php echo $data->amountPay; ?>"><?php echo $data->amountPay; ?>
														&nbsp;-&nbsp;<?php echo $data->description; ?>
													</option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Payment Mode</label>
											<select class="form-control" id="type" name="type">
												<option value="choose">Please Select One....</option>
												<option value="Cash">Cash</option>
												<option value="M-Pesa">M-Pesa</option>
												<option value="Cheque">Cheque</option>
												<option value="Bank Deposit">Bank Deposit</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Amount Paid</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Amount Paid", "name" => "amount",
												"required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Payment Code</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Payment Code", "name" => "code")) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Physical Receipt Number</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Physical Receipt Number", "name" => "fnumber",
												"required" => "true", "type" => "text")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Added By</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Location", "name" => "addedby", "readonly" => "true",
												"value" => $this->session->userdata('name'))) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<?php if ($view_data != null) { ?>
												<label>Share Holder Name</label>
												<?php echo form_input(array("class" => "form-control",
													"placeholder" => "Shareholder", "name" => "holder",
													"readonly" => "true", "value" => $view_data['Name'])) ?>
											<?php } ?>
										</div>
									</div>
									<div class="col-md-4">
										<?php if ($view_data != null) { ?>
											<?php echo form_hidden('Id', $view_data['Id'], 'class="form-control"'); ?>
										<?php } ?>
										<?php if ($view_data != null) { ?>
											<?php echo form_hidden('idnumber', $view_data['IdNo'],
												'class="form-control"'); ?>
										<?php } ?>
										<?php if ($view_data != null) { ?>
											<?php echo form_hidden('kra', $view_data['krapin'], 'class="form-control"'); ?>
										<?php } ?>
										<?php if ($view_data != null) { ?>
											<?php echo form_hidden('typeuser', $view_data['type'],
												'class="form-control"'); ?>
										<?php } ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<?php if ($view_data != null) { ?>
												<?php echo form_submit('add', 'Add Registration Payment',
													'class="btn btn-success pull-right margin-right"'); ?>
											<?php } ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<button type="reset" class="btn btn-danger pull-right">Cancel</button>
										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
